#pragma once
#include <functional>
#include <iostream>


namespace Engine {

    class Point 
    {
    private:
        int x;
        int y;

    public:
        Point();
        Point(const int x, const int y);
        Point(const Point& other);
        Point(Point &&other);

        int X() const;
        int Y() const;

        Point CopyWithOffset(const int x, const int y) const;
        Point& Scale(const int factor);
        Point& Scale(const int factorX, const int factorY);
        Point& Translate(const int x, const int y);
        Point& Translate(const Point &other);

        bool operator==(const Point& rhs) const;

        Point& operator=(const Point &other);
        Point& operator=(Point &&other);
    };

    std::ostream& operator<<(std::ostream &output, const Point &point);
}

namespace std {

    template<>
    struct hash<Engine::Point> {
        size_t operator()(const Engine::Point& point) const;
    };
}