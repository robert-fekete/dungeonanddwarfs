#include "SdlKeyboardState.h"


namespace SdlWrapper {

    SdlKeyboardState::SdlKeyboardState(const unsigned char state[], const int numberOfKeys)
        : state(state),
        numberOfKeys(numberOfKeys)
    {}

    bool SdlKeyboardState::IsPressed(Events::Scancode key) const {

        if ((int)key >= numberOfKeys) {

            return false;
        }

        return state[key] == 1;
    }
}