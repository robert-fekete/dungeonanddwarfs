#pragma once
#include "Point.h"
#include "Selection.h"

namespace Engine {

    class ITextureResource
    {
    public:
        virtual void Draw(const Selection &selection, const double angle, const Point &position) const = 0;
        virtual void Draw(const Selection &selection, const double angle, const Point &position, size_t width, size_t height) const = 0;
        virtual int GetWidth() const = 0;
        virtual int GetHeight() const = 0;
        virtual ~ITextureResource() {}
    };
}
