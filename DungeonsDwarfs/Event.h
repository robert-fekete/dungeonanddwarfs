#pragma once
#include "IEventHandler.h"

namespace Engine {

    class Event
    {
    public:
        virtual void Accept(const IEventHandler& handler) const {};
    };
}
