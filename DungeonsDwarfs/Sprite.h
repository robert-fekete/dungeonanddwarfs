#pragma once

#include <memory>
#include "ITextureResource.h"
#include "Selection.h"

// TODO Remove
#include "SDL.h"
#include "SdlTextureResource.h"

namespace Engine {

    class Sprite 
    {
    private:
        const std::shared_ptr<ITextureResource> texture;
        const Selection selection;
        const double angle;

    public:
        // TODO figure something out for the bloated constructors... 2^N constructor for N property. Builder pattern?
        Sprite(const std::shared_ptr<ITextureResource> texture);
        Sprite(const std::shared_ptr<ITextureResource> texture, const double angle);
        Sprite(const std::shared_ptr<ITextureResource> texture, const Selection selection);
        Sprite(const std::shared_ptr<ITextureResource> texture, const double angle, const Selection selection);
        void Draw(const Point &position) const;
        void Draw(const Point &position, size_t width, size_t height) const;

        // TODO REMOVE
        SDL_Renderer* GetRenderer() const;
    };
}