#include "Point.h"


namespace Engine {

    Point::Point()
        : x(0), y(0)
    {}

    Point::Point(const int x, const int y)
        : x(x), y(y)
    {}

    Point::Point(const Point& other)
        : x(other.x), y(other.y)
    {}

    Point::Point(Point &&other)
        : x(other.x), y(other.y)
    {}

    int Point::X() const {

        return x;
    }

    int Point::Y() const {

        return y;
    }

    Point Point::CopyWithOffset(const int x, const int y) const {

        Point other(*this);
        other.Translate(x, y);

        return other;
    }

    Point& Point::Scale(const int factor) {

        x *= factor;
        y *= factor;

        return *this;
    }

    Point& Point::Scale(const int factorX, const int factorY) {

        x *= factorX;
        y *= factorY;

        return *this;
    }

    Point& Point::Translate(const int x, const int y) {

        this->x += x;
        this->y += y;

        return *this;
    }

    Point& Point::Translate(const Point &other) {

        return Translate(other.X(), other.Y());
    }

    bool Point::operator==(const Point& rhs) const {

        return x == rhs.x && y == rhs.y;
    }

    Point& Point::operator=(const Point &other) {

        x = other.X();
        y = other.Y();

        return *this;
    }

    Point& Point::operator=(Point &&other) {

        x = other.X();
        y = other.Y();

        return *this;
    }

    std::ostream& operator<<(std::ostream &output, const Point &point) {

        output << point.X() << ":" << point.Y();
        return output;
    }
}

namespace std {

    size_t hash<Engine::Point>::operator()(const Engine::Point& point) const
    {
        size_t result = 17;
        result = result * 31 + hash<int>()(point.X());
        result = result * 31 + hash<int>()(point.Y());

        return result;
    }
}