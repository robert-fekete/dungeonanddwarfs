#include "Color.h"

namespace Engine {

    Color::Color()
        : r(0), g(0), b(0), alpha(0)
    {}

    Color::Color(const unsigned char r, const unsigned char g, const unsigned char b, const unsigned char alpha)
        : r(r), g(g), b(b), alpha(alpha)
    {}

    Color::Color(const Color& other)
        : r(other.r), g(other.g), b(other.b), alpha(other.alpha)
    {}

    Color::Color(Color &&other)
        : r(other.r), g(other.g), b(other.b), alpha(other.alpha)
    {}

    unsigned char Color::GetR() const {

        return r;
    }

    unsigned char Color::GetG() const {

        return g;
    }

    unsigned char Color::GetB() const {

        return b;
    }

    unsigned char Color::GetAlpha() const {
    
        return alpha;
    }

    bool Color::operator==(const Color& rhs) const {

        return r == rhs.r &&
            g == rhs.g &&
            b == rhs.b &&
            alpha == rhs.alpha;
    }

    Color& Color::operator=(const Color &other) {

        r = other.r;
        g = other.g;
        b = other.b;
        alpha = other.alpha;

        return *this;
    }

    Color& Color::operator=(Color &&other) {

        r = other.r;
        g = other.g;
        b = other.b;
        alpha = other.alpha;

        return *this;
    }
}