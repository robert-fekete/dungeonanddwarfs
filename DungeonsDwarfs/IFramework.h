#pragma once
#include <memory>
#include "BaseResourceFactory.h"
#include "IKeyboardProvider.h"
#include "Color.h"
#include "Event.h"

namespace Engine {

    class IFramework 
    {
    public:
        virtual size_t GetScreenWidth() const = 0;
        virtual size_t GetScreenHeight() const = 0;

        virtual void SetBackgroundColor(const Color color) = 0;
        virtual void BeforeDraw() = 0;
        virtual void AfterDraw() = 0;
        virtual std::unique_ptr<Event> PollEvents() = 0;
        virtual unsigned int GetTicks() = 0;
        virtual std::shared_ptr<BaseResourceFactory> GetResourceFactory() const = 0;
        virtual std::shared_ptr<IKeyboardProvider> GetKeyboardProvider() const = 0;
        virtual ~IFramework() {};
    };
}