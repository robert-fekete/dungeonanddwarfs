#include "Interpolator.h"

namespace Engine {

    Interpolator::Interpolator(const int start, const int end, const unsigned int timeSpan)
        : start(start),
        end(end),
        timeSpan(timeSpan) {}

    void Interpolator::Update(unsigned int dt) {

        timeEllapsed += dt;
    }

    void Interpolator::Reset(const int start_, const int end_, const unsigned int timeSpan_) {

        start = start_;
        end = end_;
        timeSpan = timeSpan_;
        timeEllapsed = 0;
    }

    bool Interpolator::IsFinished() const {

        return timeEllapsed >= timeSpan;
    }

    int Interpolator::GetValue() const {

        if (IsFinished()) {

            return end;
        }
        if (start == end) {

            return end;
        }

        auto processStatus = ((double)timeEllapsed) / timeSpan;
        auto interpolationDone = processStatus * (end - start);

        return (int)interpolationDone + start;
    }
}