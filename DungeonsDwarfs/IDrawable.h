#pragma once

namespace Engine {

    class IDrawable {

    public:
        virtual ~IDrawable() {};
        virtual void Draw() const = 0;
    };
}