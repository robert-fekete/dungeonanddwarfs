#include "SdlTextureResource.h"

#include <iostream>

namespace SdlWrapper {

    SdlTextureResource::SdlTextureResource(SDL_Renderer *renderer, SDL_Texture* texture_, const Engine::Logger &logger_)
        : renderer(renderer), 
        logger(logger_), 
        texture(texture_, std::bind(&SdlTextureResource::ReleaseTexture, this, std::placeholders::_1))
    {
        SDL_QueryTexture(texture.get(), NULL, NULL, &width, &height);
    }

    int SdlTextureResource::GetWidth() const {

        return width;
    }

    int SdlTextureResource::GetHeight() const {

        return height;
    }

    void SdlTextureResource::Draw(const Engine::Selection &selection, const double angle, const Engine::Point &position) const {

        Draw(selection, angle, position, (size_t) width, (size_t) height);
    }

    void SdlTextureResource::Draw(const Engine::Selection &selection, const double angle, const Engine::Point &position, size_t width, size_t height) const {
        
        SDL_Rect imageDestination;
        imageDestination.x = position.X();
        imageDestination.y = position.Y();
        imageDestination.w = (int)width;
        imageDestination.h = (int)height;

        auto sourceSelection = ConvertToSdl(selection);

        SDL_RenderCopyEx(renderer, texture.get(), &sourceSelection, &imageDestination, angle, NULL, SDL_FLIP_NONE);
    }

    SDL_Rect SdlTextureResource::ConvertToSdl(const Engine::Selection &selection) const {

        SDL_Rect rectangle;
        rectangle.x = selection.X;
        rectangle.y = selection.Y;
        rectangle.w = (int) selection.Width;
        rectangle.h = (int) selection.Height;

        return rectangle;
    }

    void SdlTextureResource::ReleaseTexture(SDL_Texture *texture) const {
    
        if (texture != nullptr) {
            logger.LogMessage("Releasing resources: ", "Texture");
            SDL_DestroyTexture(texture);
            texture = nullptr;
        }
    }
}