#include "SdlFramework.h"

// Engine 
#include "KeyBoardEvent.h"
#include "MouseClickEvent.h"
#include "MouseMoveEvent.h"
#include "QuitEvent.h"


namespace SdlWrapper {
    
    SdlFramework::SdlFramework(std::ostream &loggerOutput, const size_t screenWidth, const size_t screenHeight)
        : logger(Engine::Logger(loggerOutput)),
        window(nullptr, std::bind(&SdlFramework::ReleaseWindow, this, std::placeholders::_1)),
        renderer(nullptr, std::bind(&SdlFramework::ReleaseRenderer, this, std::placeholders::_1)),
        screenWidth(screenWidth),
        screenHeight(screenHeight),
        keyboardProvider(new SdlKeyboardProvider())
    {
        InitializeSdl();
        CreateWindow();
        CreateRenderer();

        // TODO CLEAN
        SDL_SetRenderDrawBlendMode(renderer.get(), SDL_BLENDMODE_BLEND);

        // Initializing BaseResourceFactory
        resourceFactory.reset(new SdlResourceFactory(renderer.get(), logger));
    }

    void SdlFramework::InitializeSdl() {

        if (SDL_Init(SDL_INIT_VIDEO) != 0) {

            logger.LogMessage("SDL_Init failed: ", SDL_GetError());
            throw std::runtime_error("SDL_Init Failed");
        }
        isSdlInitialized = true;

        if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
            logger.LogMessage("IMG_Init failed: ", SDL_GetError());
            throw std::runtime_error("IMG_Init Failed");
        }
    }

    void SdlFramework::CreateWindow() {

        auto window_ptr = SDL_CreateWindow("Title", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, (int) screenWidth, (int) screenHeight, SDL_WINDOW_SHOWN);
        if (window_ptr == nullptr) {

            logger.LogMessage("SDL_CreateWindow failed: ", SDL_GetError());
            throw std::runtime_error("SDL_CreateWindow Failed");
        }
        else {
            window.reset(window_ptr);
            window_ptr = nullptr;
        }
    }

    void SdlFramework::CreateRenderer() {

        auto renderer_ptr = SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (renderer_ptr == nullptr) {

            logger.LogMessage("SDL_CreateRenderer failed: ", SDL_GetError());
            throw std::runtime_error("SDL_CreateRenderer Failed");
        }
        else {
            renderer.reset(renderer_ptr);
            renderer_ptr = nullptr;
        }
    }

    void SdlFramework::SetBackgroundColor(const Engine::Color color) {

        backgroundColor = color;
    }

    size_t SdlFramework::GetScreenWidth() const {

        return screenWidth;
    }

    size_t SdlFramework::GetScreenHeight() const {

        return screenHeight;
    }

    void SdlFramework::BeforeDraw() {

        SDL_SetRenderDrawColor(renderer.get(), backgroundColor.GetR(), backgroundColor.GetG(), backgroundColor.GetB(), backgroundColor.GetAlpha());
        SDL_RenderClear(renderer.get());
    }

    void SdlFramework::AfterDraw() {

        SDL_RenderPresent(renderer.get());
    }

    std::unique_ptr<Engine::Event> SdlFramework::PollEvents() {

        SDL_Event event;
        std::unique_ptr<Engine::Event> ptr;
        ptr.reset(new Engine::Event());

        if (SDL_PollEvent(&event) == 0) {
            return ptr;
        }
        else {

            switch (event.type) {
                
            case SDL_QUIT:
                ptr.reset(new Engine::QuitEvent());
                break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
                
                ptr.reset(new Engine::KeyboardEvent((Events::Key)(int)event.key.keysym.sym, 
                    event.key.keysym.mod, 
                    event.key.state == SDL_PRESSED, 
                    event.key.repeat > 0));
                break;

            case SDL_MOUSEBUTTONUP:
            case SDL_MOUSEBUTTONDOWN:
                ptr.reset(new Engine::MouseClickEvent(event.button.x, 
                    event.button.y, 
                    event.button.clicks, 
                    event.button.button == SDL_BUTTON_LEFT, 
                    event.button.state == SDL_PRESSED));
                break;

            case SDL_MOUSEMOTION:
                ptr.reset(new Engine::MouseMoveEvent(event.motion.x,
                    event.motion.y,
                    (event.motion.state & SDL_BUTTON_LMASK) == SDL_BUTTON_LMASK,
                    (event.motion.state & SDL_BUTTON_RMASK) == SDL_BUTTON_RMASK));
                break;
            }

            return ptr;
        }
    }

    unsigned int SdlFramework::GetTicks() {

        return SDL_GetTicks();
    }

    std::shared_ptr<Engine::BaseResourceFactory> SdlFramework::GetResourceFactory() const {

        return resourceFactory;
    }

    std::shared_ptr<Engine::IKeyboardProvider> SdlFramework::GetKeyboardProvider() const {
        
        return keyboardProvider;
    }

    void SdlFramework::ReleaseRenderer(SDL_Renderer* renderer) const{

        if (renderer != nullptr) {
            logger.LogMessage("Releasing resources: ", "Renderer");
            SDL_DestroyRenderer(renderer);
            renderer = nullptr;
        }
    }

    void SdlFramework::ReleaseWindow(SDL_Window* window) const{
        
        if (window != nullptr) {
            logger.LogMessage("Releasing resources: ", "Window");
            SDL_DestroyWindow(window);
            window = nullptr;
        }
    }

    SdlFramework::~SdlFramework()
    {
        if (isSdlInitialized) {
            logger.LogMessage("Releasing resources: ", "SDL Init");
            SDL_Quit();
            isSdlInitialized = false;
        }
    }
}