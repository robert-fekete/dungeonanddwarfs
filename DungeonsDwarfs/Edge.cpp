#include "Edge.h"


namespace Graphs {

    Edge::Edge()
        : from(0, 0),
        to(0, 0)
    {}

    Edge::Edge(const Engine::Point from, const Engine::Point to)
        : from(from),
        to(to)
    {}

    Edge::Edge(const Edge& other)
        : from(other.from),
        to(other.to)
    {}

    Edge::Edge(Edge &&other)
        : from(other.from),
        to(other.to)
    {}

    Engine::Point Edge::From() const {

        return from;
    }

    Engine::Point Edge::To() const {

        return to;
    }

    bool Edge::operator==(const Edge& rhs) const {

        return (from == rhs.from && to == rhs.to) ||
            (from == rhs.to && to == rhs.from);
    }

    Edge& Edge::operator=(const Edge &other) {

        from = other.from;
        to = other.to;

        return *this;
    }

    Edge& Edge::operator=(Edge &&other) {

        from = other.from;
        to = other.to;

        return *this;
    }

    std::ostream& operator<<(std::ostream &output, const Edge &edge) {

        output << edge.From() << "->" << edge.To();
        return output;
    }
}


namespace std {

    size_t hash<Graphs::Edge>::operator()(const Graphs::Edge& edge) const
    {
        size_t result = 17;
        result = result * 31 
            + hash<Engine::Point>()(edge.From()) 
            + hash<Engine::Point>()(edge.To());

        return result;
    }
}