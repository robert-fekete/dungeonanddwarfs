#pragma once
#include <functional>
#include <utility>
#include <vector>

// Engine
#include "Animation.h"
#include "IKeyboardProvider.h"
#include "IUpdatable.h"
#include "Interpolator.h"

// DungeonDwarfs
#include "Pawn.h"

namespace DungeonDwarfs {

    class PawnViewModel : public Engine::IUpdatable
    {
    private:
        const unsigned int moveTime = 500;
        const unsigned int stuckTime = 200;

        const size_t tileSize;
        
        const std::shared_ptr<Engine::IKeyboardProvider> keyboardProvider;
        Pawn &pawn;

        bool isMoving = false;
        unsigned int state = 0;
        Engine::Interpolator interpolator{ 0, 0, 0 };

        std::function<Engine::Point()> positionGetter;

    public:
        PawnViewModel(Pawn &pawn, std::shared_ptr<Engine::IKeyboardProvider> keyboardProvider, const size_t tileSize);

        void Update(unsigned int dt) override;

        void MoveUp();
        void MoveDown();
        void MoveLeft();
        void MoveRight();
        Engine::Point GetPosition() const;
        unsigned int GetState() const;

    private:
        Engine::Point DefaultGetPosition() const;
        void MoveHorizontally(std::function<void()> move, unsigned int nextState);
        void MoveVertically(std::function<void()> move, unsigned int nextState);
        void StartMoving(std::function<int(Engine::Point)> coordinateSelector,
                         std::function<void()> move, 
                         unsigned int nextState, 
                         std::function<Engine::Point()> positionGetter);
    };
}
