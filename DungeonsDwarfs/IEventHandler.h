#pragma once

namespace Engine {

    class KeyboardEvent;
    class MouseClickEvent;
    class MouseMoveEvent;
    class QuitEvent;

    class IEventHandler {

    public:
        virtual void Handle(const KeyboardEvent &e) const {};
        virtual void Handle(const MouseClickEvent &e) const {};
        virtual void Handle(const MouseMoveEvent &e) const {};
        virtual void Handle(const QuitEvent &e) const = 0;
        virtual ~IEventHandler() {};
    };
}