#include "Map.h"
#include <algorithm>

namespace DungeonDwarfs {

    Map::Map(const DrawableTileFactory tileFactory, const Graphs::Graph graph_, size_t width, size_t height)
        : 
        graph(graph_),
        tileFactory(tileFactory),
        width(width),
        height(height)
    {
        for (const auto& vertex : graph.GetVertices()) {
            
            tiles[vertex] = std::make_unique<Tile>(vertex, graph, false);
            // TODO Fix
            //drawableTiles.push_back(tileFactory.CreateTile(*tiles[vertex].get()));
        }
    }

    void Map::Draw() const {

        for (const auto &tile : drawableTiles) {

            tile.Draw();
        }
    }
}