#pragma once
#include <memory>
#include "IUpdatable.h"
#include <vector>

namespace Engine {

    class UpdatablesCollection
    {
    private:
        std::vector<IUpdatable*> updatables;

    public:
        UpdatablesCollection();
        void UpdateAll(unsigned int dt);
        void Add(IUpdatable* updatable);
        ~UpdatablesCollection();
    };
}
