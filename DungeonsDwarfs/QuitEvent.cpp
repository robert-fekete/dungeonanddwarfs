#include "QuitEvent.h"

namespace Engine {

    void QuitEvent::Accept(const IEventHandler &handler) const {

        handler.Handle(*this);
    }
}