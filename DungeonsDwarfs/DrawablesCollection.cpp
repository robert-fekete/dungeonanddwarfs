#include "DrawablesCollection.h"


namespace Engine {
    
    DrawablesCollection::DrawablesCollection()
    { }


    void DrawablesCollection::DrawAll() {

        for (const auto &drawable : drawables) {

            drawable->Draw();
        }
    }

    void DrawablesCollection::Add(IDrawable* drawable) {

        drawables.push_back(std::move(drawable));
    }

    void DrawablesCollection::Clear() {

        drawables.clear();
    }
}