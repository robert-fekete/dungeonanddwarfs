#include "DrawableShadow.h"


namespace DungeonDwarfs {

    // TODO clean up logic, does this really need both view model and model?
    DrawableShadow::DrawableShadow(const PawnViewModel &pawnViewModel,
                                    const Pawn &pawn,
                                    const Graphs::Graph &map,
                                    Engine::Sprite center,
                                    std::vector<Engine::Sprite> endings,
                                    std::vector<Engine::Sprite> sides,
                                    const size_t tileSize,
                                    const size_t screenHeight)
        : tileSize(tileSize),
        screenHeight(screenHeight),
        pawnViewModel(pawnViewModel),
        pawn(pawn),
        map(map),
        center(center),
        endings(endings),
        sides(sides)
    {}

    void DrawableShadow::Draw() const {

        auto viewPosition = pawnViewModel.GetPosition();
        viewPosition.Scale(1, -1).
            Translate(0, (int)screenHeight - (int)tileSize);
        //    .Translate(-6, -10)); TODO Might want to translate to align with tunnel wall

        auto modelPosition = pawn.GetPosition();
        center.Draw(viewPosition, tileSize, tileSize);
        DrawTop(viewPosition, modelPosition);
        DrawRight(viewPosition, modelPosition);
        DrawBottom(viewPosition, modelPosition);
        DrawLeft(viewPosition, modelPosition);
    }

    void DrawableShadow::DrawTop(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const {

        DrawNeighbor(viewPosition, viewPosition.CopyWithOffset(0, -(int) tileSize), modelPosition, modelPosition.CopyWithOffset(0, 1), 0);
    }

    void DrawableShadow::DrawRight(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const {

        DrawNeighbor(viewPosition, viewPosition.CopyWithOffset((int)tileSize, 0), modelPosition, modelPosition.CopyWithOffset(1, 0), 1);
    }

    void DrawableShadow::DrawBottom(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const {

        DrawNeighbor(viewPosition, viewPosition.CopyWithOffset(0, (int)tileSize), modelPosition, modelPosition.CopyWithOffset(0, -1), 2);
    }

    void DrawableShadow::DrawLeft(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const {

        DrawNeighbor(viewPosition, viewPosition.CopyWithOffset(-(int)tileSize, 0), modelPosition, modelPosition.CopyWithOffset(-1, 0), 3);
    }

    void DrawableShadow::DrawNeighbor(const Engine::Point &centerPosition, const Engine::Point &viewPosition, const Engine::Point &modelPosition, const Engine::Point &neighbor, int spriteIndex) const {

        if (map.HasEdge(modelPosition, neighbor)) {
            endings[spriteIndex].Draw(viewPosition, tileSize, tileSize);
        }
        else {
            sides[spriteIndex].Draw(centerPosition, tileSize, tileSize);
        }
    }
}