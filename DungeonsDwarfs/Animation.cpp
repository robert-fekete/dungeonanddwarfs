#include "Animation.h"

namespace Engine {

    Animation::Animation(const std::shared_ptr<ITextureResource> texture, std::vector<Selection> frameSelections, unsigned int fps)
        : millisecPerFrame(1000 / fps) {

        for (const auto selection : frameSelections) {

            frames.push_back(Sprite(texture, selection));
        }
    }

    void Animation::Animate(unsigned int dt) {

        timeSinceLastFrame += dt;
        while (timeSinceLastFrame > millisecPerFrame) {

            timeSinceLastFrame -= millisecPerFrame;            
            IncrementFrameIterator();
        }
    }

    void Animation::Draw(const Point &position) const {

        frames[frameIterator].Draw(position);
    }

    void Animation::Draw(const Point &position, size_t width, size_t height) const {

        frames[frameIterator].Draw(position, width, height);
    }

    void Animation::IncrementFrameIterator() {

        frameIterator++;
        if (frameIterator >= frames.size()) {
            frameIterator -= (unsigned int) frames.size();
        }
    }
}