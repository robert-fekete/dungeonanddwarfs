#include "PawnViewModel.h"


namespace DungeonDwarfs {

    PawnViewModel::PawnViewModel(Pawn &pawn, std::shared_ptr<Engine::IKeyboardProvider> keyboardProvider, const size_t tileSize)
        : tileSize(tileSize),
        keyboardProvider(keyboardProvider),
        pawn(pawn),
        positionGetter(std::bind(&PawnViewModel::DefaultGetPosition, this))
    {
    }

    void PawnViewModel::Update(unsigned int dt) {
        
        if (interpolator.IsFinished()) {

            isMoving = false;
            positionGetter = std::bind(&PawnViewModel::DefaultGetPosition, this);
            state = 0;
        }
        else {

            interpolator.Update(dt);
        }

        if (!isMoving) {

            const auto state = keyboardProvider->GetState();
            if (state->IsPressed(Events::Scancode::SCANCODE_LEFT) || state->IsPressed(Events::Scancode::SCANCODE_A)){
                MoveLeft();
            }
            if (state->IsPressed(Events::Scancode::SCANCODE_RIGHT) || state->IsPressed(Events::Scancode::SCANCODE_D)) {
                MoveRight();
            }
            if (state->IsPressed(Events::Scancode::SCANCODE_UP) || state->IsPressed(Events::Scancode::SCANCODE_W)) {
                MoveUp();
            }
            if (state->IsPressed(Events::Scancode::SCANCODE_DOWN) || state->IsPressed(Events::Scancode::SCANCODE_S)) {
                MoveDown();
            }
        }
    }

    void PawnViewModel::MoveUp() {

        MoveVertically(std::bind(&Pawn::MoveUp, &pawn), 1);
    }

    void PawnViewModel::MoveDown() {

        MoveVertically(std::bind(&Pawn::MoveDown, &pawn), 3);
    }

    void PawnViewModel::MoveLeft() {

        MoveHorizontally(std::bind(&Pawn::MoveLeft, &pawn), 4);
    }

    void PawnViewModel::MoveRight() {

        MoveHorizontally(std::bind(&Pawn::MoveRight, &pawn), 2);
    }

    Engine::Point PawnViewModel::GetPosition() const {

        return positionGetter();
    }

    unsigned int PawnViewModel::GetState() const {

        return state;
    }

    void PawnViewModel::MoveHorizontally(std::function<void()> move, unsigned int nextState) {

        StartMoving([](Engine::Point position) { return position.X(); }, 
            move, 
            nextState, 
            [this]() {
                return Engine::Point(interpolator.GetValue(), 
                                     pawn.GetPosition().Scale((int)tileSize).Y());
            });
    }

    void PawnViewModel::MoveVertically(std::function<void()> move, unsigned int nextState) {

        StartMoving([](Engine::Point position) { return position.Y(); }, 
            move, 
            nextState, 
            [this]() { 
                return Engine::Point(pawn.GetPosition().Scale((int)tileSize).X(), 
                                     interpolator.GetValue());
            });
    }

    // LowPrio TODO Clean up this horrible delegate logic? state machine?
    void PawnViewModel::StartMoving(std::function<int(Engine::Point)> coordinateSelector, std::function<void()> move, unsigned int nextState, std::function<Engine::Point()> positionGetter_) {
        
        if (isMoving) {
            return;
        }

        auto before = coordinateSelector(pawn.GetPosition()) * (int)tileSize;
        move();
        auto after = coordinateSelector(pawn.GetPosition()) * (int) tileSize;

        auto isStuck = before == after;
        auto interpolationTime = isStuck ? stuckTime : moveTime;
        isMoving = true;
        state = nextState;
        interpolator.Reset(before, after, interpolationTime);
        positionGetter = positionGetter_;
    }

    Engine::Point PawnViewModel::DefaultGetPosition() const {

        return pawn.GetPosition().Scale((int) tileSize);
    }
}