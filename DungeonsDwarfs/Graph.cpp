#include "Graph.h"


namespace Graphs {
    
    Graph::Graph(std::unordered_set<Engine::Point> vertices_, std::unordered_set<Edge> edges_)
        :vertices(vertices_)
    {
        for (const auto &vertex : vertices) {

            edges[vertex] = std::unordered_set<Engine::Point>();
        }

        for (const auto &edge : edges_) {

            edges[edge.From()].insert(edge.To());
            edges[edge.To()].insert(edge.From());
        }
    }

    std::unordered_set<Engine::Point> Graph::GetVertices() const {

        return vertices;
    }

    bool Graph::HasEdge(const Engine::Point &from, const Engine::Point &to) const {

        // TODO This is crashes for some reason if graph is empty!?
        auto fromIter = edges.find(from);
        if (fromIter == edges.end()) {
            return false;
        }
        auto toIter = fromIter->second.find(to);

        return toIter != std::end(fromIter->second);
    }
}