#include "GraphSerializer.h"
#include <sstream>

namespace Graphs {

    GraphSerializer::GraphSerializer(const std::string path)
        : path(path)
    {}

    void GraphSerializer::Serialize(const std::unordered_set<Engine::Point> &vertices, 
        const std::unordered_set<Edge> &edges) const {

        auto outputStream = std::ofstream(path);
        if (!outputStream.is_open()) {

            throw std::runtime_error("Could not open file for serialization: " + path);
        }

        outputStream << vertices.size() << std::endl;
        for (const auto& vertex : vertices) {

            outputStream << vertex.X() << coordinateSeparator << vertex.Y() << std::endl;
        }

        outputStream << edges.size() << std::endl;
        for (const auto& edge : edges) {

            outputStream << edge.From().X() << coordinateSeparator << edge.From().Y() <<
                pointSeparator << edge.To().X() << coordinateSeparator << edge.To().Y() << std::endl;
        }
    }

    void GraphSerializer::Deserialize(std::unordered_set<Engine::Point> &vertices,
        std::unordered_set<Edge> &edges) const {

        auto inputStream = std::ifstream(path);
        if (!inputStream.is_open()) {

            throw std::runtime_error("Could not open file for de-serialization: " + path);
        }

        if (inputStream.peek() == std::ifstream::traits_type::eof()) {
            return;
        }

        std::string token;
        std::getline(inputStream, token);
        unsigned int noVertices = std::stoi(token);
        vertices.reserve(noVertices);

        for (unsigned int i = 0; i < noVertices; i++) {

            std::string line;
            std::getline(inputStream, line);

            std::stringstream lineParser{ line };
            std::string x, y;
            std::getline(lineParser, x, coordinateSeparator);
            std::getline(lineParser, y);

            vertices.insert(Engine::Point(std::stoi(x), std::stoi(y)));
        }

        getline(inputStream, token);
        unsigned int noEdges = std::stoi(token);
        edges.reserve(noEdges);

        for (unsigned int i = 0; i < noEdges; i++) {

            std::string line;
            std::getline(inputStream, line);

            std::stringstream lineParser{ line };
            std::string x1, y1, x2, y2;
            std::getline(lineParser, x1, coordinateSeparator);
            std::getline(lineParser, y1, pointSeparator);
            std::getline(lineParser, x2, coordinateSeparator);
            std::getline(lineParser, y2);

            Engine::Point from(std::stoi(x1), std::stoi(y1));
            Engine::Point to(std::stoi(x2), std::stoi(y2));
            edges.insert(Edge(from, to));
        }
    }
}