#include "MouseClickEvent.h"


namespace Engine {

    MouseClickEvent::MouseClickEvent(int x, int y, unsigned int clicks, bool isLeft, bool isPressed)
        : X(x), Y(y), Clicks(clicks), IsLeft(isLeft), IsPressed(isPressed) {}

    void MouseClickEvent::Accept(const IEventHandler &handler) const {

        handler.Handle(*this);
    }
}