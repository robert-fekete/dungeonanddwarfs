#include "MouseMoveEvent.h"


namespace Engine {

    MouseMoveEvent::MouseMoveEvent(int x, int y, bool isLeftPressed, bool isRightPressed)
        : X(x),
        Y(y),
        IsLeftPressed(isLeftPressed),
        IsRightPressed(isRightPressed)
    {}

    void MouseMoveEvent::Accept(const IEventHandler &handler) const {

        handler.Handle(*this);
    }
}