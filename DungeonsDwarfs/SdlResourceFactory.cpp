#include "SdlResourceFactory.h"
#include "SdlRectangleShape.h"


namespace SdlWrapper {

    SdlResourceFactory::SdlResourceFactory(SDL_Renderer *renderer, const Engine::Logger &logger)
        : BaseResourceFactory(logger),
        renderer(renderer)
    {
    }

    std::unique_ptr<Engine::IShape> SdlResourceFactory::GetRectangleShape(const Engine::Color color, const bool isFilled) const {

        return std::unique_ptr<Engine::IShape>(new SdlRectangleShape(renderer, color, isFilled));
    }

    std::shared_ptr<Engine::ITextureResource> SdlResourceFactory::CreateImageResource(const std::string &path) {

        return std::shared_ptr<Engine::ITextureResource>(new SdlTextureResource(renderer, LoadTexture(path), logger));
    }
    
    SDL_Texture* SdlResourceFactory::LoadTexture(const std::string &path) {

        auto ptr = IMG_LoadTexture(renderer, path.c_str());
        if (ptr == nullptr) {

            logger.LogMessage("SDL_CreateTexture failed for " + path + ". ", SDL_GetError());
            throw std::runtime_error("SDL_CreateTexture failed for " + path + ". " + SDL_GetError());
        }

        return ptr;
    }
}