#pragma once
#include <functional>
#include <iostream>

// Engine
#include "Point.h"


namespace Graphs {

    class Edge
    {
    private:
        Engine::Point from;
        Engine::Point to;

    public:
        Edge();
        Edge(const Engine::Point from, const Engine::Point to);
        Edge(const Edge& other);
        Edge(Edge &&other);

        Engine::Point From() const;
        Engine::Point To() const;

        bool operator==(const Edge& rhs) const;

        Edge& operator=(const Edge &other);
        Edge& operator=(Edge &&other);
    };

    std::ostream& operator<<(std::ostream &output, const Edge &edge);
}

namespace std {

    template<>
    struct hash<Graphs::Edge> {
        size_t operator()(const Graphs::Edge& edge) const;
    };
}