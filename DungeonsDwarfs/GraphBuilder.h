#pragma once
#include <memory>
#include <vector>
#include <utility>

// Engine
#include "Point.h"

// Graphs
#include "Edge.h"
#include "Graph.h"
#include "GraphSerializer.h"

namespace Graphs {

    class GraphBuilder 
    {
    private:
        std::unordered_set<Engine::Point> vertices;
        std::unordered_set<Edge> edges;

    public:
        GraphBuilder();

        bool HasVertex(const Engine::Point &vertex) const ;
        void AddVertex(const Engine::Point vertex);
        void RemoveVertex(const Engine::Point vertex);

        bool HasEdge(const Engine::Point &from, const Engine::Point &to) const;
        void AddEdge(const Engine::Point from, const Engine::Point to);
        void RemoveEdge(const Engine::Point from, const Engine::Point to);

        void Serialize(const GraphSerializer &serializer) const;
        static GraphBuilder Deserialize(const GraphSerializer &serializer);
        Graph Build() const;

    private:
        GraphBuilder(std::unordered_set<Engine::Point> vertices, std::unordered_set<Edge> edges);
        void RemoveRelatedEdges(const Engine::Point vertex);
        std::unordered_set<Engine::Point>::const_iterator FindVertex(const Engine::Point &vertex) const;
        std::unordered_set<Edge>::const_iterator FindEdge(const Engine::Point &from, const Engine::Point &to) const;
    };
}
