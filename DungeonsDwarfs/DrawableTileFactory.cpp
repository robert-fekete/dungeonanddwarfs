#include "DrawableTileFactory.h"
#include "Sprite.h"

namespace DungeonDwarfs {

    DrawableTileFactory::DrawableTileFactory(std::shared_ptr<Engine::BaseResourceFactory> resourceFactory, const size_t tileSize, const size_t screenHeight)
        : resourceFactory(resourceFactory),
        tileSize(tileSize),
        screenHeight(screenHeight)
    { }

    DrawableTile DrawableTileFactory::CreateTile(const TileViewModel &tile) const {

        return DrawableTile(tile, GetFloorSprite(tile), GetShadowSprites(tile), tileSize, screenHeight);
    }
    
    Engine::Sprite DrawableTileFactory::GetFloorSprite(const TileViewModel &tile) const {

        if (tile.IsVoid) {
            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(0, 0, floorTileSize, floorTileSize));
        }
        else {
            auto position = tile.GetPosition();
            auto top = tile.HasNeighbor(position.CopyWithOffset(0, 1));
            auto right = tile.HasNeighbor(position.CopyWithOffset(1, 0));
            auto bottom = tile.HasNeighbor(position.CopyWithOffset(0, -1));
            auto left = tile.HasNeighbor(position.CopyWithOffset(-1, 0));

            if (top) {
                if (right) {
                    if (bottom) {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(0 * floorTileSize, 4 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(3 * floorTileSize, 3 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                    else {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(2 * floorTileSize, 3 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(0 * floorTileSize, 2 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                }
                else {
                    if (bottom) {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(1 * floorTileSize, 3 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(3 * floorTileSize, 1 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                    else {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(2 * floorTileSize, 2 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(0 * floorTileSize, 1 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                }
            }
            else {
                if (right) {
                    if (bottom) {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(0 * floorTileSize, 3 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(3 * floorTileSize, 2 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                    else {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(2 * floorTileSize, 1 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(1 * floorTileSize, 1 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                }
                else {
                    if (bottom) {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(1 * floorTileSize, 2 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(2 * floorTileSize, 0 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                    else {
                        if (left) {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(3 * floorTileSize, 0 * floorTileSize, floorTileSize, floorTileSize));
                        }
                        else {
                            return Engine::Sprite(resourceFactory->GetImageResource(floorSheetPath), Engine::Selection(1 * floorTileSize, 0 * floorTileSize, floorTileSize, floorTileSize));
                        }
                    }
                }
            }
        }
    }

    std::vector<Engine::Sprite> DrawableTileFactory::GetShadowSprites(const TileViewModel &tile) const {
        
        std::vector<Engine::Sprite> sprites;
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(0 * shadowTileSize, 2 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(0 * shadowTileSize, 0 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(1 * shadowTileSize, 0 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(2 * shadowTileSize, 0 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(3 * shadowTileSize, 0 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(0 * shadowTileSize, 1 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(1 * shadowTileSize, 1 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(2 * shadowTileSize, 1 * shadowTileSize, shadowTileSize, shadowTileSize)));
        sprites.push_back(Engine::Sprite(resourceFactory->GetImageResource(shadowSheetPath), Engine::Selection(3 * shadowTileSize, 1 * shadowTileSize, shadowTileSize, shadowTileSize)));
        
        return sprites;
    }
}