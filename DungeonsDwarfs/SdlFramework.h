#pragma once
#include <iostream>
#include <functional>
#include <memory>
#include <vector>
#include "SDL.h"
#include "SDL_image.h"

// Engine
#include "Event.h"
#include "Logger.h"
#include "IDrawable.h"
#include "IFramework.h"

// SqlWrapper
#include "SdlResourceFactory.h"
#include "SdlKeyboardProvider.h"

namespace SdlWrapper {

    class SdlFramework : public Engine::IFramework
    {
    private:
        const size_t screenWidth;
        const size_t screenHeight;
        Engine::Color backgroundColor;

        bool isSdlInitialized = false;
        std::unique_ptr<SDL_Window, std::function<void(SDL_Window*)>> window;
        std::unique_ptr<SDL_Renderer, std::function<void(SDL_Renderer*)>> renderer;
        std::shared_ptr<Engine::BaseResourceFactory> resourceFactory;
        std::shared_ptr<Engine::IKeyboardProvider> keyboardProvider;
        const Engine::Logger logger;

    public:
        SdlFramework(std::ostream &loggerOutput, const size_t screenWidth, const size_t screenHeight);

        size_t GetScreenWidth() const override;
        size_t GetScreenHeight() const override;

        void SetBackgroundColor(const Engine::Color color) override;
        void BeforeDraw() override;
        void AfterDraw() override;
        std::unique_ptr<Engine::Event> PollEvents() override;
        unsigned int GetTicks() override;
        std::shared_ptr<Engine::BaseResourceFactory> GetResourceFactory() const override;
        std::shared_ptr<Engine::IKeyboardProvider> GetKeyboardProvider() const override;

        ~SdlFramework();

    private:
        void InitializeSdl();
        void CreateWindow();
        void CreateRenderer();

        void ReleaseRenderer(SDL_Renderer* renderer) const;
        void ReleaseWindow(SDL_Window* window) const;
    };
}

