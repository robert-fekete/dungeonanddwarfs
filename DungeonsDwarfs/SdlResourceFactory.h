#pragma once
#include <map>
#include "SDL.h"
#include "SDL_image.h"

// Engine
#include "BaseResourceFactory.h"
#include "ITextureResource.h"
#include "Selection.h"
#include "Logger.h"

// SdlWrapper
#include "SdlTextureResource.h"


namespace SdlWrapper {

    class SdlResourceFactory : public Engine::BaseResourceFactory
    {
    private:
        SDL_Renderer *renderer;

    public:
        SdlResourceFactory(SDL_Renderer *renderer, const Engine::Logger &logger);
        std::unique_ptr<Engine::IShape> GetRectangleShape(const Engine::Color color, const bool isFilled) const override;

    private:
        SDL_Texture* LoadTexture(const std::string &path);
        std::shared_ptr<Engine::ITextureResource> CreateImageResource(const std::string &path) override;
    };
}

