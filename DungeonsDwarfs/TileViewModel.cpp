#include "TileViewModel.h"


namespace DungeonDwarfs {

    TileViewModel::TileViewModel(const Tile tile_,
        const Pawn &pawn,
        const Graphs::Graph &graph)
        : tile(tile_),
        pawn(pawn),
        graph(graph),
        interpolator(0, 0, 0),
        IsVoid(false)
    {
        isVisible = tile.HasNeighbor(pawn.GetPosition());
    }

    void TileViewModel::Update(unsigned int dt) {

        if (interpolator.IsFinished()) {
            auto isUpdated = SetVisible(IsVisible());
            if (isUpdated) {
                interpolator.Update(dt);
            }
        }
        else {
            interpolator.Update(dt);
        }
    }

    bool TileViewModel::SetVisible(bool value) {

        if (isVisible == value) {
            return false;
        }

        isVisible = value;
        StartShadowAnimation();
        return true;
    }

    void TileViewModel::StartShadowAnimation() {

        if (isVisible) {
            interpolator.Reset(255, 0, 500);
        }
        else {
            interpolator.Reset(0, 255, 500);
        }
    }

    Engine::Point TileViewModel::GetPosition() const {

        return tile.GetPosition();
    }

    bool TileViewModel::HasNeighbor(const Engine::Point &other) const {

        return tile.HasNeighbor(other);
    }

    bool TileViewModel::IsVisible() const {

        auto newVisible = tile.HasNeighbor(pawn.GetPosition()) ||
            tile.GetPosition() == pawn.GetPosition();
        
        return newVisible;
    }

    bool TileViewModel::HasShadow() const {
        
        return !(pawn.GetPosition() == tile.GetPosition());
    }

    int TileViewModel::GetShadowDirection() const {

        auto pawnPos = pawn.GetPosition();
        auto tilePos = tile.GetPosition();

        if (pawnPos.X() == tilePos.X()) {
            if (pawnPos.Y() == tilePos.Y()) {
                return 0;
            }
            else if (pawnPos.Y() > tilePos.Y()) {
                return 3;
            }
            else {
                return 1;
            }
        }
        else {
            if (pawnPos.X() > tilePos.X()) {
                return 4;
            }
            else {
                return 2;
            }
        }
    }

    int TileViewModel::GetShadowLevel() const {

        if (!isVisible) {
            return 255;
        }
        //return interpolator.GetValue();
        return 0;
    }
}