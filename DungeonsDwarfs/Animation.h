#pragma once
#include <vector>
#include <memory>
#include "ITextureResource.h"
#include "Selection.h"
#include "Sprite.h"

namespace Engine {

    class Animation
    {
    private:
        std::vector<Sprite> frames;
        const unsigned int millisecPerFrame;

        unsigned int frameIterator = 0;
        unsigned int timeSinceLastFrame = 0;
    public:
        Animation(const std::shared_ptr<ITextureResource> texture, std::vector<Selection> frames, unsigned int fps);
        void Animate(unsigned int dt);
        void Draw(const Point &position) const;
        void Draw(const Point &position, size_t width, size_t height) const;
    private:
        void IncrementFrameIterator();
    };
}