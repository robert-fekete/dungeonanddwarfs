#pragma once
#include <utility>

// Engine
#include "Point.h"

// Graphs
#include "Graph.h"

namespace DungeonDwarfs {

    class Pawn
    {
    private:
        Engine::Point position;
        const Graphs::Graph graph;

    public:
        Pawn(Engine::Point coordinate, const Graphs::Graph graph);
        Engine::Point GetPosition() const;
        void MoveUp();
        void MoveDown();
        void MoveLeft();
        void MoveRight();

    private:
        void Move(Engine::Point next);
    };
}
