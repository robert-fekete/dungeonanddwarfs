#pragma once

// Engine
#include "Interpolator.h"
#include "IUpdatable.h"

// DungeonDwarfs
#include "Graph.h"
#include "Pawn.h"
#include "Point.h"
#include "Tile.h"


namespace DungeonDwarfs {

    class TileViewModel : public Engine::IUpdatable
    {
    public:
        bool IsVoid;
    private:
        const Tile tile;
        const Pawn &pawn;
        const Graphs::Graph &graph;

        bool isVisible;
        Engine::Interpolator interpolator;
    public:
        TileViewModel(const Tile tile, const Pawn &pawn, const Graphs::Graph &graph);

        void Update(unsigned int dt) override;

        Engine::Point GetPosition() const;
        bool HasNeighbor(const Engine::Point &other) const;
        bool IsVisible() const;
        bool HasShadow() const;
        int GetShadowDirection() const;
        int GetShadowLevel() const;
    private:
        bool SetVisible(bool value);
        void StartShadowAnimation();
    };
}