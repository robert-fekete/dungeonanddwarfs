#pragma once
#include <memory>
#include <vector>

#include "IDrawable.h"
#include "IShape.h"
#include "Sprite.h"
#include "TileViewModel.h"

namespace DungeonDwarfs {

    class DrawableTile : public Engine::IDrawable
    {
    private:
        const size_t tileSize;
        const size_t screenHeight;

        const TileViewModel &tile;
        const Engine::Sprite sprite;
        const std::vector<Engine::Sprite> shadowSprites;
    public:
        DrawableTile(const TileViewModel &tile,
            const Engine::Sprite sprite,
            const std::vector<Engine::Sprite> shadowSprites,
            const size_t tileSize, 
            const size_t screenHeight);
        void Draw() const override;
    private:
        int GetShadowIndex(int direction) const;
    };
}

