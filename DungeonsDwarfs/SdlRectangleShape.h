#pragma once
#include "SDL.h"

// Engine
#include "Color.h"
#include "IShape.h"

namespace SdlWrapper {

    class SdlRectangleShape : public Engine::IShape
    {
    private:
        const Engine::Color color;
        const bool isFilled;
        SDL_Renderer *renderer;
    public:
        SdlRectangleShape(SDL_Renderer *renderer, const Engine::Color color, bool isFilled);

        void Draw(const int x, const int y, const size_t width, const size_t height) const override;
    };
}