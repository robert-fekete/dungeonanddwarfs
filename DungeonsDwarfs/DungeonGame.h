#pragma once
#include <vector>
#include <memory>

// Engine
#include "Color.h"
#include "IDrawable.h"
#include "Game.h"

// Graphs
#include "Graph.h"

// DungeonDwarfs
#include "DrawableTileFactory.h"
#include "DrawablePawnFactory.h"
#include "DrawableShadowFactory.h"
#include "EventHandler.h"
#include "Tile.h"
#include "Pawn.h"
#include "PawnViewModel.h"

namespace DungeonDwarfs {
 
    class DungeonGame : public Engine::Game
    {
    private:
        const size_t tileSize = 64;

        const Graphs::Graph map;
        DrawableTileFactory tileFactory;
        DrawablePawnFactory pawnFactory;
        DrawableShadowFactory shadowFactory;
        EventHandler eventHandler;

        std::vector<std::unique_ptr<TileViewModel>> tiles;
        std::vector<DrawableTile> drawableTiles;
        Pawn pawn;
        PawnViewModel pawnViewModel;
        std::unique_ptr<DrawablePawn> drawablePawn;
        std::unique_ptr<DrawableShadow> drawableShadow;

    public:
        DungeonGame(std::unique_ptr<Engine::IFramework> framework, Graphs::Graph map);

        ~DungeonGame();

    private:
        void RegisterTiles();
        void RegisterPawn();
        void RegisterShadow();
        const Engine::IEventHandler& GetEventHandler() const override;
    };
}
