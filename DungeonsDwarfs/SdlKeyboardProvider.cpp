#include "SdlKeyboardProvider.h"


namespace SdlWrapper {

    std::unique_ptr<Events::IKeyboardState> SdlKeyboardProvider::GetState() const {

        int numberOfKeys;
        auto state = SDL_GetKeyboardState(&numberOfKeys);

        return std::unique_ptr<Events::IKeyboardState>(new SdlKeyboardState(state, numberOfKeys));
    }
}