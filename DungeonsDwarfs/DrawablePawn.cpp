#include "DrawablePawn.h"


namespace DungeonDwarfs {

    DrawablePawn::DrawablePawn(const PawnViewModel &viewModel, 
                                Engine::Sprite neutral, 
                                std::vector<Engine::Animation> animation, 
                                const size_t tileSize, 
                                const size_t screenHeight)
        : tileSize(tileSize),
        screenHeight(screenHeight),
        viewModel(viewModel),
        neutral(neutral),
        animation(animation)
    {
    }

    void DungeonDwarfs::DrawablePawn::Draw() const {

        auto position = viewModel.GetPosition();
        position.Scale(1, -1).
            Translate(0, (int) screenHeight - (int)tileSize).
            Translate((int)(tileSize - width) / 2, (int)(tileSize - height) / 2 - 10);

        // LowPrio TODO Clean up this int=state logic? using state machine?
        auto state = viewModel.GetState();
        if (state == 0) {
            neutral.Draw(position, width, height);
        }
        else {
            animation[GetCurrentAnimationIndex(state)].Draw(position, width, height);
        }
    }
    
    int DungeonDwarfs::DrawablePawn::GetCurrentAnimationIndex(unsigned int state) const {

        if (state == 1) {
            return 3;
        }
        else if (state == 2) {
            return 2;
        }
        else if (state == 3) {
            return 0;
        }
        else if (state == 4) {
            return 1;
        }
        else {
            throw std::runtime_error("Invalid state: " + state);
        }
    }

    void DrawablePawn::Update(unsigned int dt) {

        auto state = viewModel.GetState();
        if (state != 0) {
            animation[GetCurrentAnimationIndex(state)].Animate(dt);
        }
    }

    DungeonDwarfs::DrawablePawn::~DrawablePawn()
    {
    }
}