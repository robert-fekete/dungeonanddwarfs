#pragma once
#include <memory>
#include <string>
#include <vector>
#include "BaseResourceFactory.h"
#include "Color.h"
#include "DrawableTile.h"
#include "TileViewModel.h"


namespace DungeonDwarfs {

    class DrawableTileFactory
    {
    public:
        //const Engine::Color BackgroundColor{ 98, 60, 20, 255 };
        const Engine::Color BackgroundColor{ 255, 255, 255, 255 };
    private:
        const std::string floorSheetPath{ "..\\Resources\\Cave_TileSet.png" };
        const std::string shadowSheetPath{ "..\\Resources\\ShadowSpriteSheet.png" };
        const int floorTileSize = 64;
        const int shadowTileSize = 64;

        const size_t tileSize;
        const size_t screenHeight;
        std::shared_ptr<Engine::BaseResourceFactory> resourceFactory;

    public:
        DrawableTileFactory(std::shared_ptr<Engine::BaseResourceFactory> resourceFactory, const size_t tileSize, const size_t screenHeight);
        DrawableTile CreateTile(const TileViewModel &tile) const;

    private:
        Engine::Sprite GetFloorSprite(const TileViewModel &tile) const;
        std::vector<Engine::Sprite> GetShadowSprites(const TileViewModel &tile) const;
    };
}
