#pragma once
#include <iostream>
#include <string>

namespace Engine {

    class Logger
    {
    public:
        Logger(std::ostream &output);

        void LogMessage(const std::string &message, const std::string &detail) const;
        void LogException(const std::exception &exception, const std::string &detail) const;

    private:
        std::ostream &output;
    };
}
