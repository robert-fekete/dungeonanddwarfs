#pragma once
#include <functional>

// Engine
#include "IEventHandler.h"
#include "KeyboardEvent.h"
#include "QuitEvent.h"

// DungeonDwarfs
#include "PawnViewModel.h"

namespace DungeonDwarfs {

    class EventHandler : public Engine::IEventHandler
    {
    private:
        PawnViewModel &pawn;
        std::function<void()> quitAction;
    public:
        EventHandler(std::function<void()> quitAction, PawnViewModel &pawn);
        void Handle(const Engine::KeyboardEvent &e) const override;
        void Handle(const Engine::QuitEvent &e) const override;
    };
}
