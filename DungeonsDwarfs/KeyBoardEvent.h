#pragma once
#include "Event.h"
#include "IEventHandler.h"
#include "KeyboardConstants.h"

namespace Engine {
 
    class KeyboardEvent : public Event
    {
    public:
        Events::Key Key;

        // LowPrio TODO Hide these properties and provide functions to access these
        unsigned int Modifiers;
        bool IsPressed;
        bool Repeat;
    public:
        KeyboardEvent(Events::Key key, unsigned int modifiers, bool state, bool repeat);
        void Accept(const IEventHandler &handler) const override;
    };
}
