#include "Pawn.h"


namespace DungeonDwarfs {
    Pawn::Pawn(Engine::Point coordinate, const Graphs::Graph graph)
        : position(coordinate),
        graph(graph)
    {
    }

    Engine::Point Pawn::GetPosition() const {

        return position;
    }

    void Pawn::MoveUp() {

        Move(position.CopyWithOffset(0, 1));
    }

    void Pawn::MoveDown() {

        Move(position.CopyWithOffset(0, -1));
    }

    void Pawn::MoveLeft() {

        Move(position.CopyWithOffset(-1, 0));
    }

    void Pawn::MoveRight() {

        Move(position.CopyWithOffset(1, 0));
    }

    void Pawn::Move(Engine::Point next) {

        if (graph.HasEdge(position, next)) {
            position = next;
        }
    }
}