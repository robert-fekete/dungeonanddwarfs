#pragma once


namespace Engine {

    class Color
    {
    private:
        unsigned char r;
        unsigned char g;
        unsigned char b;
        unsigned char alpha;

    public:
        Color();
        Color(const unsigned char r, const unsigned char g, const unsigned char b, const unsigned char alpha);
        Color(const Color& other);
        Color(Color &&other);

        unsigned char GetR() const;
        unsigned char GetG() const;
        unsigned char GetB() const;
        unsigned char GetAlpha() const;

        bool operator==(const Color& rhs) const;

        Color& operator=(const Color &other);
        Color& operator=(Color &&other);
    };
}