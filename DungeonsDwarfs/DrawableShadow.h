#pragma once
#include <vector>

// Engine
#include "IDrawable.h"
#include "Point.h"
#include "Sprite.h"

// Graphs
#include "Graph.h"

// DungeonDwarfs
#include "Pawn.h"
#include "PawnViewModel.h"

namespace DungeonDwarfs {

    class DrawableShadow : public Engine::IDrawable
    {
    private:
        const size_t tileSize;
        const size_t screenHeight;

        const PawnViewModel &pawnViewModel;
        const Pawn &pawn;
        const Graphs::Graph &map;
        const Engine::Sprite center;
        const std::vector<Engine::Sprite> endings;
        const std::vector<Engine::Sprite> sides;

    public:
        DrawableShadow(const PawnViewModel &pawnViewModel,
            const Pawn &pawn,
            const Graphs::Graph &map,
            Engine::Sprite center, 
            std::vector<Engine::Sprite> endings, 
            std::vector<Engine::Sprite> sides,
            const size_t tileSize,
            const size_t screenHeight);
        void Draw() const override;

    private:
        void DrawTop(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const;
        void DrawRight(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const;
        void DrawBottom(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const;
        void DrawLeft(const Engine::Point &viewPosition, const Engine::Point &modelPosition) const;
        void DrawNeighbor(const Engine::Point &centerPosition,
            const Engine::Point &viewPosition,
            const Engine::Point &modelPosition,
            const Engine::Point &neighbor,
            int spriteIndex) const;
    };
}