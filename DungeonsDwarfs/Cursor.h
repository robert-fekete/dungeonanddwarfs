#pragma once
#include <memory>

// Engine
#include "Point.h"
#include "Selection.h"
#include "IDrawable.h"
#include "IShape.h"

namespace DungeonDwarfsMapEditor {

    class Cursor : public Engine::IDrawable
    {
    private:
        const unsigned int displayMarginSize = 10;
        const size_t tileSize;
        const size_t screenHeight;

        const std::unique_ptr<Engine::IShape> drawing;
        Engine::Selection rectangle;
        bool isVisible;
    public:
        Cursor(std::unique_ptr<Engine::IShape> drawing, const size_t tileSize, const size_t screenHeight);
        void Hide();
        void Show();
        void SetTileTop(const Engine::Point &point);
        void SetTileRight(const Engine::Point &point);
        void SetTileBottom(const Engine::Point &point);
        void SetTileLeft(const Engine::Point &point);
        void SetTileCenter(const Engine::Point &point);
        void Draw() const override;
    private:
        void UpdateRectangle(const Engine::Selection rectangle);

    };
}