#include "GraphBuilder.h"
#include <algorithm>

namespace Graphs {

    GraphBuilder::GraphBuilder()
    {}

    GraphBuilder::GraphBuilder(std::unordered_set<Engine::Point> vertices,
        std::unordered_set<Edge> edges)
        : vertices(vertices),
        edges(edges)
    {}

    bool GraphBuilder::HasVertex(const Engine::Point &vertex) const {

        return FindVertex(vertex) != std::end(vertices);
    }

    void GraphBuilder::AddVertex(const Engine::Point vertex) {

        vertices.insert(vertex);
    }

    void GraphBuilder::RemoveVertex(const Engine::Point vertex) {

        auto iter = FindVertex(vertex);
        if (iter == std::end(vertices)) {
            return;
        }

        vertices.erase(iter);
        RemoveRelatedEdges(vertex);
    }

    std::unordered_set<Engine::Point>::const_iterator GraphBuilder::FindVertex(const Engine::Point &vertex) const {

        return vertices.find(vertex);
    }

    void GraphBuilder::RemoveRelatedEdges(const Engine::Point vertex) {

        for (auto it{ std::begin(edges) }; it != std::end(edges); /* nothing */) {

            if (it->From() == vertex || it->To() == vertex) {
                it = edges.erase(it);
            }
            else {
                ++it;
            }
        }
    }

    bool GraphBuilder::HasEdge(const Engine::Point &from, const Engine::Point &to) const {

        return FindEdge(from, to) != std::end(edges);
    }

    void GraphBuilder::AddEdge(const Engine::Point from, const Engine::Point to) {

        edges.insert(Edge(from, to));
    }

    void GraphBuilder::RemoveEdge(const Engine::Point from, const Engine::Point to) {

        auto iter = FindEdge(from, to);
        if (iter == std::end(edges)) {
            return;
        }

        edges.erase(iter);
    }

    std::unordered_set<Edge>::const_iterator GraphBuilder::FindEdge(const Engine::Point &from, const Engine::Point &to) const {

        return edges.find(Edge(from, to));
    }

    void GraphBuilder::Serialize(const GraphSerializer &serializer) const {

        serializer.Serialize(vertices, edges);
    }

    GraphBuilder GraphBuilder::Deserialize(const GraphSerializer &serializer) {

        std::unordered_set<Engine::Point> vertices;
        std::unordered_set<Edge> edges;
        serializer.Deserialize(vertices, edges);

        return GraphBuilder(vertices, edges);
    }

    Graph GraphBuilder::Build() const {

        return Graph(vertices, edges);
    }
}