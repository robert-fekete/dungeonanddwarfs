#pragma once
#include <memory>

// Engine
#include "Color.h"
#include "Game.h"
#include "EventHandler.h"

// DungeonDwarfs
#include "DrawableTileFactory.h"
#include "Map.h"
#include "Tile.h"

// DungeonDwardsMapEditor
#include "Cursor.h"
#include "GraphBuilder.h"
#include "MapEditorEventHandler.h"

using namespace DungeonDwarfs;

namespace DungeonDwarfsMapEditor {

    class MapEditor : public Engine::Game
    {
    private:
        const size_t tileSize = 64;
        const size_t width = 10;
        const size_t height = 10;
        const Engine::Color cursorColor;

        std::shared_ptr<Engine::BaseResourceFactory> resourceFactory;
        DrawableTileFactory tileFactory;

        Graphs::GraphBuilder &graphBuilder;
        std::unique_ptr<Map> map;
        Cursor cursor;

        MapEditorEventHandler eventHandler;

    public:
        MapEditor(std::unique_ptr<Engine::IFramework> framework, Graphs::GraphBuilder &graphBuilder);

    private:
        void RebuildMap();
        void AddDrawables();
        const Engine::IEventHandler& GetEventHandler() const override;
    };
}
