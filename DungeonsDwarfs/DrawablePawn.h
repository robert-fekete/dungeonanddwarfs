#pragma once
#include <memory>
#include <vector>
#include "IDrawable.h"
#include "IUpdatable.h"
#include "Animation.h"
#include "Sprite.h"
#include "PawnViewModel.h"

namespace DungeonDwarfs {

    class DrawablePawn : public Engine::IDrawable, public Engine::IUpdatable
    {
    private:
        const size_t tileSize;
        const size_t screenHeight;

        const PawnViewModel &viewModel;
        Engine::Sprite neutral;
        std::vector<Engine::Animation> animation;
        const size_t width = 32;
        const size_t height = 48;

    public:
        DrawablePawn(const PawnViewModel &viewModel, Engine::Sprite neutral, std::vector<Engine::Animation> animation, const size_t tileSize, const size_t screenHeight);
        void Draw() const override;
        void Update(unsigned int dt) override;
        ~DrawablePawn();

    private:
        int GetCurrentAnimationIndex(unsigned int state) const;
    };
}