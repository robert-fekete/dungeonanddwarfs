#include "EventHandler.h"

namespace DungeonDwarfs {

    EventHandler::EventHandler(std::function<void()> quitAction, PawnViewModel &pawn)
        : quitAction(quitAction), pawn(pawn)
    { }

    bool IsSignalToQuit(const Engine::KeyboardEvent &e) {

        return (e.Key == Events::Key::q || e.Key == Events::Key::ESCAPE) && !e.IsPressed;
    }
    
    void EventHandler::Handle(const Engine::KeyboardEvent &e) const {

        if (IsSignalToQuit(e)) {
            quitAction();
        }
    }

    void EventHandler::Handle(const Engine::QuitEvent &e) const {

        quitAction();
    }
}
