#include "DrawableShadowFactory.h"
#include <vector>

// Engine
#include "Sprite.h"

namespace DungeonDwarfs {

    DrawableShadowFactory::DrawableShadowFactory(std::shared_ptr<Engine::BaseResourceFactory> resourceFactory,
                                                const size_t tileSize,
                                                const size_t screenHeight)
        : tileSize(tileSize),
        screenHeight(screenHeight),
        resourceFactory(resourceFactory)
    {}

    std::unique_ptr<DrawableShadow> DrawableShadowFactory::CreateShadow(const PawnViewModel &pawnViewModel, const Pawn &pawn, const Graphs::Graph &map) const {

        auto texture = resourceFactory->GetImageResource(spriteSheetPath);
        Engine::Sprite center{ texture, Engine::Selection(0, 2 * (int)tileSize, tileSize, tileSize) };

        std::vector<Engine::Sprite> endings;
        std::vector<Engine::Sprite> sides;
        for (int i = 0; i < 4; i++) {

            endings.emplace_back(texture, Engine::Selection(i * (int)tileSize, 0, tileSize, tileSize));
            sides.emplace_back(texture, Engine::Selection(i * (int)tileSize, 1 * (int)tileSize, tileSize, tileSize));
        }

        return std::make_unique<DrawableShadow>(pawnViewModel, pawn, map, center, endings, sides, tileSize, screenHeight);
    }
}