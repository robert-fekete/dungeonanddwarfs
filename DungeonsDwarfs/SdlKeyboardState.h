#pragma once
#include "IKeyboardState.h"

namespace SdlWrapper {

    class SdlKeyboardState : public Events::IKeyboardState
    {
    private:
        const unsigned char *state;
        const int numberOfKeys;

    public:
        SdlKeyboardState(const unsigned char state[], const int numberOfKeys);
        bool IsPressed(Events::Scancode key) const override;
    };
}