#pragma once
#include <memory>
#include "IKeyboardState.h"

namespace Engine {

    class IKeyboardProvider
    {
    public:
        virtual std::unique_ptr<Events::IKeyboardState> GetState() const = 0;
    };
}