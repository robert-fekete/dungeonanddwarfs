#pragma once
#include <memory>
#include "IDrawable.h"
#include <vector>

namespace Engine {

    class DrawablesCollection
    {
    private:
        std::vector<IDrawable*> drawables;

    public:
        DrawablesCollection();
        void DrawAll();
        void Add(IDrawable* drawable);
        void Clear();
    };
}
