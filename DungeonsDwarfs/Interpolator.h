#pragma once
#include "IUpdatable.h"

namespace Engine {

    class Interpolator : public IUpdatable
    {
    private:
        int start;
        int end;
        unsigned int timeSpan;
        unsigned int timeEllapsed = 0;

    public:

        Interpolator(const int start, const int end, const unsigned int timeSpan);

        void Update(unsigned int dt) override;
        void Reset(const int start, const int end, const unsigned int timeSpan);

        bool IsFinished() const;
        int GetValue() const;
    };
}