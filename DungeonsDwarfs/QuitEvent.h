#pragma once
#include "Event.h"
#include "IEventHandler.h"

namespace Engine {

    class QuitEvent : public Event
    {
    public:
        void Accept(const IEventHandler &handler) const override;
    };
}
