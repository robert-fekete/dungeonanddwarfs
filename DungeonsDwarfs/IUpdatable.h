#pragma once

namespace Engine {

    class IUpdatable {

    public:
        virtual ~IUpdatable() {};
        virtual void Update(unsigned int dt) = 0;
    };
}