#include "BaseResourceFactory.h"

namespace Engine {

    BaseResourceFactory::BaseResourceFactory(const Logger &logger)
        : logger(logger) {}

    // LowPrio TODO Clear up the cache if using too much memory
    std::shared_ptr<ITextureResource> BaseResourceFactory::GetImageResource(const std::string &path) {

        try {
            auto iter = cache.find(path);
            if (iter != cache.end()) {
                return iter->second;
            }

            auto ptr = CreateImageResource(path);
            cache[path] = ptr;

            return ptr;
        }
        catch (std::runtime_error &exception) {
            logger.LogMessage("Failed to create Image resource", exception.what());
            throw;
        }
    }
}