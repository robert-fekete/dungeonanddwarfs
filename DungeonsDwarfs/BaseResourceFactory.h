#pragma once
#include <string>
#include <functional>
#include <map>

// Engine
#include "IShape.h"
#include "ITextureResource.h"
#include "Color.h"
#include "Logger.h"

namespace Engine {

    class BaseResourceFactory 
    {
    private:
        std::map<std::string, std::shared_ptr<Engine::ITextureResource>> cache;

    protected:
        const Logger &logger;

    public:
        BaseResourceFactory(const Logger &logger);
        
        std::shared_ptr<ITextureResource> GetImageResource(const std::string &path);
        virtual std::unique_ptr<IShape> GetRectangleShape(const Color color, const bool isFilled) const = 0;

        virtual ~BaseResourceFactory() {}

    private:
        virtual std::shared_ptr<ITextureResource> CreateImageResource(const std::string &path) = 0;
    };
}