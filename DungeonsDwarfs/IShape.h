#pragma once

// Engine
#include "Color.h"

namespace Engine {

    class IShape 
    {
    public:
        virtual void Draw(const int x, const int y, const size_t width, const size_t height) const = 0;
    };
}