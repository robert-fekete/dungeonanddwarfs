#pragma once
#include <memory>
#include "SDL.h"

// Engine
#include "IKeyboardProvider.h"

// SdlWrapper
#include "SdlKeyboardState.h"


namespace SdlWrapper {

    class SdlKeyboardProvider : public Engine::IKeyboardProvider
    {
    public:
        std::unique_ptr<Events::IKeyboardState> GetState() const override;
    };
}