#include "MapEditor.h"


namespace DungeonDwarfsMapEditor {

    MapEditor::MapEditor(std::unique_ptr<Engine::IFramework> framework_, Graphs::GraphBuilder &graphBuilder)
        : Game(std::move(framework_)),
        cursorColor(0, 255, 0, 255),
        resourceFactory(framework->GetResourceFactory()),
        tileFactory(resourceFactory, tileSize, framework->GetScreenHeight()),
        graphBuilder(graphBuilder),
        map(new Map(tileFactory, graphBuilder.Build(), width, height)),
        cursor(resourceFactory->GetRectangleShape(cursorColor, false), tileSize, framework->GetScreenHeight()),
        eventHandler(MapEditorEventHandler(std::bind(&MapEditor::Stop, this), 
            std::bind(&MapEditor::RebuildMap, this), 
            graphBuilder, 
            cursor,
            tileSize, 
            framework->GetScreenHeight()))
    {
        framework->SetBackgroundColor(tileFactory.BackgroundColor);
        AddDrawables();
    }

    void MapEditor::RebuildMap() {

        drawables.Clear();
        map.reset(new Map(tileFactory, graphBuilder.Build(), width, height));
        AddDrawables();
    }

    void MapEditor::AddDrawables() {

        drawables.Add(map.get());
        drawables.Add(&cursor);
    }

    const Engine::IEventHandler& MapEditor::GetEventHandler() const {

        return eventHandler;
    }
}