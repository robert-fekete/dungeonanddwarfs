#include "UpdatablesCollection.h"


namespace Engine {

    UpdatablesCollection::UpdatablesCollection()
    { }


    void UpdatablesCollection::UpdateAll(unsigned int dt) {

        for (const auto &updatable : updatables) {

            updatable->Update(dt);
        }
    }

    void UpdatablesCollection::Add(IUpdatable* updatable) {

        updatables.push_back(std::move(updatable));
    }

    UpdatablesCollection::~UpdatablesCollection()
    {
    }
}