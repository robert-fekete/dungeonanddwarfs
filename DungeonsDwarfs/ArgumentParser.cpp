#include "ArgumentParser.h"
#include <sstream>


ArgumentParser::ArgumentParser(int count, char* values[], std::unordered_set<std::string> validArguments)
    : validArguments(validArguments),
    isValid(true) {
    
    for (int i = 1; i < count; i++) {

        std::string argument(values[i]);
        std::stringstream argumentParser{ argument };
        std::string name, value;
        std::getline(argumentParser, name, '=');
        std::getline(argumentParser, value);

        arguments[name] = Trim(value, "\"\' \t");

        if (validArguments.find(name) == validArguments.end()) {
            isValid = false;
            return;
        }
    }
}

std::string ArgumentParser::Trim(const std::string& input, const std::string& trimCharacter) const
{
    const auto beginning = input.find_first_not_of(trimCharacter);
    if (beginning == std::string::npos)
        return ""; 

    const auto ending = input.find_last_not_of(trimCharacter);
    const auto range = ending - beginning + 1;

    return input.substr(beginning, range);
}

bool ArgumentParser::IsValid() const {

    return isValid;
}

bool ArgumentParser::HasArgument(const std::string &name) const {

    return arguments.find(name) != arguments.end();
}

std::string ArgumentParser::GetArgumentValue(const std::string &name) const {

    auto iter = arguments.find(name);
    if (iter == arguments.end()) {

        throw std::logic_error("Argument wasn't found");
    }

    return iter->second;
}
