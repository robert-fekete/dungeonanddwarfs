#pragma once
#include <memory>

// Engine
#include "BaseResourceFactory.h"

// Graphs
#include "Graph.h"

// DungeonDwarfs
#include "DrawableShadow.h"

namespace DungeonDwarfs {

    class DrawableShadowFactory
    {
    private:
        const std::string spriteSheetPath{ "..\\Resources\\ShadowSpriteSheet.png" };

        const size_t tileSize;
        const size_t screenHeight;
        std::shared_ptr<Engine::BaseResourceFactory> resourceFactory;

    public:
        DrawableShadowFactory(std::shared_ptr<Engine::BaseResourceFactory> resourceFactory, 
            const size_t tileSize, 
            const size_t screenHeight);
        std::unique_ptr<DrawableShadow> CreateShadow(const PawnViewModel &pawnViewModel, 
            const Pawn &pawn,
            const Graphs::Graph &map) const;
    };
}