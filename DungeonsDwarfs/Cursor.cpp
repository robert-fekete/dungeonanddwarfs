#include "Cursor.h"


namespace DungeonDwarfsMapEditor {

    Cursor::Cursor(std::unique_ptr<Engine::IShape> drawing, const size_t tileSize, const size_t screenHeight)
        : tileSize(tileSize),
        screenHeight(screenHeight),
        drawing(std::move(drawing)),
        rectangle(0, 0, 0, 0),
        isVisible(false)
    {}

    void Cursor::UpdateRectangle(const Engine::Selection rectangle_) {
        
        rectangle = rectangle_;
    }

    void Cursor::Hide() {

        isVisible = false;
    }

    void Cursor::Show() {

        isVisible = true;
    }

    void Cursor::Draw() const {

        if (isVisible) {
            drawing->Draw(rectangle.X, (int)screenHeight - rectangle.Y - (int)tileSize, rectangle.Width, rectangle.Height);
        }
    }

    void Cursor::SetTileTop(const Engine::Point &point) {

        auto viewPosition(point);
        viewPosition.Scale((int)tileSize).Translate(0, displayMarginSize);

        auto width = tileSize;
        auto height = 2 * displayMarginSize;
        
        UpdateRectangle(Engine::Selection(viewPosition.X(), viewPosition.Y(), width, height));
    }

    void Cursor::SetTileRight(const Engine::Point &point) {

        auto viewPosition(point);
        viewPosition.Scale((int)tileSize).Translate((int) tileSize - displayMarginSize, 0);

        auto width = 2 * displayMarginSize;
        auto height = tileSize;

        UpdateRectangle(Engine::Selection(viewPosition.X(), viewPosition.Y(), width, height));
    }

    void Cursor::SetTileBottom(const Engine::Point &point) {

        auto viewPosition(point);
        viewPosition.Scale((int)tileSize).Translate(0, -(int) tileSize + displayMarginSize);

        auto width = tileSize;
        auto height = 2 * displayMarginSize;

        UpdateRectangle(Engine::Selection(viewPosition.X(), viewPosition.Y(), width, height));
    }

    void Cursor::SetTileLeft(const Engine::Point &point) {

        auto viewPosition(point);
        viewPosition.Scale((int)tileSize).Translate(-(int)displayMarginSize, 0);

        auto width = 2 * displayMarginSize;
        auto height = tileSize;

        UpdateRectangle(Engine::Selection(viewPosition.X(), viewPosition.Y(), width, height));
    }

    void Cursor::SetTileCenter(const Engine::Point &point) {

        auto viewPosition(point);
        viewPosition.Scale((int)tileSize);
        
        auto width = tileSize;
        auto height = tileSize;

        UpdateRectangle(Engine::Selection(viewPosition.X(), viewPosition.Y(), width, height));
    }
}