#include "MapEditorEventHandler.h"
#include "Point.h"


namespace DungeonDwarfsMapEditor {

    MapEditorEventHandler::MapEditorEventHandler(std::function<void()> quitAction, 
                                                 std::function<void()> rebuildMap,
                                                 Graphs::GraphBuilder &graphBuilder,
                                                 Cursor &cursor,
                                                 const size_t tileSize, 
                                                 const size_t screenHeight)
        : quitAction(quitAction),
        rebuildMap(rebuildMap),
        graphBuilder(graphBuilder),
        cursor(cursor),
        tileSize(tileSize),
        screenHeight(screenHeight)
    {}

    void MapEditorEventHandler::Handle(const Engine::MouseClickEvent &e) const {

        if (!e.IsPressed) {
            return;
        }

        DispatchMouseEvent(e.X, e.Y,
            [this](const Engine::Point &point) {
            
                Engine::Point neighbour = point.CopyWithOffset(-1, 0);
                HandleMarginClick(point, neighbour);
            },
            [this](const Engine::Point &point) {

                Engine::Point neighbour = point.CopyWithOffset(1, 0);
                HandleMarginClick(point, neighbour);
            },
            [this](const Engine::Point &point) {

                Engine::Point neighbour = point.CopyWithOffset(0, 1);
                HandleMarginClick(point, neighbour);
            },
            [this](const Engine::Point &point) {

                Engine::Point neighbour = point.CopyWithOffset(0, -1);
                HandleMarginClick(point, neighbour);
            },
           [this](const Engine::Point &point) {
                
                HandleCenterClick(point);
            });

        rebuildMap();
    }

    void MapEditorEventHandler::Handle(const Engine::MouseMoveEvent &e) const {

        cursor.Hide();

        DispatchMouseEvent(e.X, e.Y,
        [this](const Engine::Point &point) {
            
            cursor.SetTileLeft(point);
            cursor.Show();
        },
        [this](const Engine::Point &point) {
            
            cursor.SetTileRight(point);
            cursor.Show();
        },
        [this](const Engine::Point &point) {
            
            cursor.SetTileTop(point);
            cursor.Show();
        },
        [this](const Engine::Point &point) {
            
            cursor.SetTileBottom(point);
            cursor.Show();
        },
        [this](const Engine::Point &point) {

            cursor.SetTileCenter(point);
            cursor.Show();
        });
    }

    void MapEditorEventHandler::DispatchMouseEvent(int x, int y,
        std::function<void(Engine::Point&)> lowerHorizontal,
        std::function<void(Engine::Point&)> higherHorizontal,
        std::function<void(Engine::Point&)> lowerVertical,
        std::function<void(Engine::Point&)> higherVertical,
        std::function<void(Engine::Point&)> center) const {

        auto coordinate = GetCoordinate(x, y);
        if (IsLowerMarginClick(x, y)) {

            Engine::Point neighbour = coordinate.CopyWithOffset(-1, 0);
            if (IsEdgeValid(coordinate, neighbour)) {
                lowerHorizontal(coordinate);
                return;
            }
        }
        
        if (IsHigherMarginClick(x, y)) {

            Engine::Point neighbour = coordinate.CopyWithOffset(1, 0);
            if (IsEdgeValid(coordinate, neighbour)) {
                higherHorizontal(coordinate);
                return;
            }
        }
        
        if (IsLowerMarginClick(y, x)) {

            Engine::Point neighbour = coordinate.CopyWithOffset(0, 1);
            if (IsEdgeValid(coordinate, neighbour)) {
                lowerVertical(coordinate);
                return;
            }
        }
        
        if (IsHigherMarginClick(y, x)) {

            Engine::Point neighbour = coordinate.CopyWithOffset(0, -1);
            if (IsEdgeValid(coordinate, neighbour)) {

                higherVertical(coordinate);
                return;
            }
        }
        
        if (IsCenter(x, y)){

            center(coordinate);
            return;
        }

        if (!graphBuilder.HasVertex(coordinate)) {
            
            center(coordinate);
            return;
        }
    }

    Engine::Point MapEditorEventHandler::GetCoordinate(const int viewX, const int viewY) const {

        auto x = viewX / (unsigned int)tileSize;
        int maxScreenHeightValue = (int)screenHeight - 1;
        auto y = (maxScreenHeightValue - viewY) / (unsigned int)tileSize;

        return Engine::Point(x, y);
    }

    bool IsInRange(const int value, const int min, const int max) {

        return min < value && value < max;
    }

    bool MapEditorEventHandler::IsLowerMarginClick(const int primary, const int secondary) const {

        int primaryInCell = primary % (unsigned int)tileSize;
        int secondaryInCell = secondary % (unsigned int)tileSize;
        int upperMargin = (unsigned int)tileSize - hitDetectionMarginSize;
        return primaryInCell < hitDetectionMarginSize &&
            IsInRange(secondaryInCell, hitDetectionMarginSize, upperMargin);
    }

    bool MapEditorEventHandler::IsHigherMarginClick(const int primary, const int secondary) const {

        int primaryInCell = primary % (unsigned int)tileSize;
        int secondaryInCell = secondary % (unsigned int)tileSize;
        int upperMargin = (unsigned int)tileSize - hitDetectionMarginSize;
        return primaryInCell > upperMargin &&
            IsInRange(secondaryInCell, hitDetectionMarginSize, upperMargin);
    }

    bool MapEditorEventHandler::IsCenter(const int x, const int y) const {

        auto xInCell = (x % (unsigned int)tileSize);
        auto yInCell = (y % (unsigned int)tileSize);
        auto upperMargin = ((unsigned int)tileSize - hitDetectionMarginSize);

        return IsInRange(xInCell, hitDetectionMarginSize, upperMargin) &&
            IsInRange(yInCell, hitDetectionMarginSize, upperMargin);
    }

    void MapEditorEventHandler::HandleMarginClick(const Engine::Point from, const Engine::Point to) const {
        
        if (IsEdgeValid(from, to)) {
            if (graphBuilder.HasEdge(from, to)) {
                graphBuilder.RemoveEdge(from, to);
            }
            else {
                graphBuilder.AddEdge(from, to);
            }
        }
        else {

            HandleCenterClick(from);
        }
    }

    void MapEditorEventHandler::HandleCenterClick(const Engine::Point point) const {

        if (graphBuilder.HasVertex(point)) {
            graphBuilder.RemoveVertex(point);
        }
        else {
            graphBuilder.AddVertex(point);
        }
    }

    bool MapEditorEventHandler::IsEdgeValid(const Engine::Point &from, const Engine::Point &to) const {

        return graphBuilder.HasVertex(from) && graphBuilder.HasVertex(to);
    }

    void MapEditorEventHandler::Handle(const Engine::QuitEvent &e) const {

        quitAction();
    }
}