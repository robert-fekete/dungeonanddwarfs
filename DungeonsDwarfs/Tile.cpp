#include "Tile.h"


namespace DungeonDwarfs {

    Tile::Tile(const Engine::Point coordinate, const Graphs::Graph &graph, bool isVoid)
        :coordinate(coordinate), 
        graph(graph),
        IsVoid(isVoid)
    {
    }

    Engine::Point Tile::GetPosition() const {

        return coordinate;
    }

    bool Tile::HasNeighbor(const Engine::Point &neighbor) const {

        return graph.HasEdge(coordinate, neighbor);
    }
}
