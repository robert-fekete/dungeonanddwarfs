#pragma once
#include <memory>
#include <utility>
#include <unordered_map>
#include <vector>

// Engine
#include "IDrawable.h"
#include "Point.h"

// DungeonDwarfs
#include "Graph.h"
#include "Tile.h"
#include "DrawableTile.h"
#include "DrawableTileFactory.h"


namespace DungeonDwarfs {

    class Map : public Engine::IDrawable
    {
    private:
        const Graphs::Graph graph;
        std::unordered_map<Engine::Point, std::unique_ptr<Tile>> tiles;
        std::vector<DrawableTile> drawableTiles;

        const DrawableTileFactory tileFactory;
        const size_t width;
        const size_t height;
    public:
        Map(const DrawableTileFactory tileFactory, const Graphs::Graph graph, size_t width, size_t height);

        void Draw() const override;
    };
}
