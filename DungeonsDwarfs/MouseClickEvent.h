#pragma once
#include "Event.h"
#include "IEventHandler.h"

namespace Engine {

    class MouseClickEvent : public Event
    {
    public:
        const int X;
        const int Y;
        unsigned int Clicks;
        const bool IsLeft;
        const bool IsPressed;

    public:
        MouseClickEvent(int x, int y, unsigned int clicks, bool isLeft, bool isPressed);
        void Accept(const IEventHandler &handler) const override;
    };
}
