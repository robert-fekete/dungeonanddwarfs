#pragma once
#include <functional>
#include <string>
#include <memory>
#include "SDL.h"
#include "SDL_image.h"

// Engine
#include "ITextureResource.h"
#include "Logger.h"
#include "Selection.h"

namespace SdlWrapper {

    class SdlTextureResource : public Engine::ITextureResource
    {
    public:
        // TODO temporary hack remove it

        SDL_Renderer * renderer;
    private:
        const Engine::Logger &logger;
        std::unique_ptr<SDL_Texture, std::function<void(SDL_Texture*)>> texture;
        int width;
        int height;
    public:
        SdlTextureResource(SDL_Renderer *renderer, SDL_Texture *texture, const Engine::Logger &logger);

        void Draw(const Engine::Selection &selection, const double angle, const Engine::Point &position) const override;
        void Draw(const Engine::Selection &selection, const double angle, const Engine::Point &position, size_t width, size_t height) const override;
        int GetWidth() const override;
        int GetHeight() const override;
    private:
        void ReleaseTexture(SDL_Texture *texture) const;

        SDL_Rect ConvertToSdl(const Engine::Selection &selection) const;
    };
}
