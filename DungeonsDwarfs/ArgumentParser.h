#pragma once
#include <string>
#include <unordered_map>
#include <unordered_set>


class ArgumentParser
{
private:
    const std::unordered_set<std::string> validArguments;
    std::unordered_map<std::string, std::string> arguments;
    bool isValid;

public:
    ArgumentParser(int count, char* values[], std::unordered_set<std::string> validArguments);
    bool IsValid() const;
    bool HasArgument(const std::string &name) const;
    std::string GetArgumentValue(const std::string &name) const;
private:
    std::string Trim(const std::string &input, const std::string &trimCharacter) const;
};

