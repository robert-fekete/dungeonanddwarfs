#include "SdlRectangleShape.h"


namespace SdlWrapper {

    SdlRectangleShape::SdlRectangleShape(SDL_Renderer* renderer, const Engine::Color color, bool isFilled)
        : color(color), 
        isFilled(isFilled),
        renderer(renderer)
    {}

    void SdlRectangleShape::Draw(const int x, const int y, const size_t width, const size_t height) const {
        
        SDL_Rect rectangle;
        rectangle.x = x;
        rectangle.y = y;
        rectangle.w = (int) width;
        rectangle.h = (int) height;

        SDL_SetRenderDrawColor(renderer, color.GetR(), color.GetG(), color.GetB(), color.GetAlpha());
        if (isFilled) {
            SDL_RenderFillRect(renderer, &rectangle);
        }
        else {
            SDL_RenderDrawRect(renderer, &rectangle);
        }
    }
}