#include "Game.h"
#include "QuitEvent.h"
namespace Engine {

    Game::Game(std::unique_ptr<IFramework> framework_)
        : framework(std::move(framework_))
    {
        currentTime = framework->GetTicks();
    }

    void Game::Run() {

        isRunning = true;

        while (isRunning) {

            auto currentEvent = framework->PollEvents();
            currentEvent->Accept(GetEventHandler());

            auto previousTime = currentTime;
            currentTime = framework->GetTicks();

            updatables.UpdateAll(currentTime - previousTime);

            framework->BeforeDraw();
            drawables.DrawAll();
            framework->AfterDraw();
        }
    }

    void Game::Stop() {

        isRunning = false;
    }
}