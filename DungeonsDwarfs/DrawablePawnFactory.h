#pragma once
#include <memory>
#include "BaseResourceFactory.h"
#include "DrawablePawn.h"
#include "PawnViewModel.h"

namespace DungeonDwarfs {

    class DrawablePawnFactory
    {
    private:
        const size_t tileSize;
        const size_t screenHeight;

        std::shared_ptr<Engine::BaseResourceFactory> resourceFactory;

    public:
        DrawablePawnFactory(std::shared_ptr<Engine::BaseResourceFactory> resourceFactory, 
            const size_t tileSize,
            const size_t screenHeight);
        std::unique_ptr<DrawablePawn> CreatePawn(PawnViewModel &pawn);
    };
}
