#include "Sprite.h"

namespace Engine {

    Sprite::Sprite(const std::shared_ptr<ITextureResource> texture)
        : texture(texture),
        angle(0.0),
        selection(0, 0, texture->GetWidth(), texture->GetHeight()) { }

    Sprite::Sprite(const std::shared_ptr<ITextureResource> texture, const double angle)
        : texture(texture),
        angle(angle),
        selection(0, 0, texture->GetWidth(), texture->GetHeight()) { }
    
    Sprite::Sprite(const std::shared_ptr<ITextureResource> texture, const Selection selection)
        : texture(texture), angle(0), selection(selection) {}

    Sprite::Sprite(const std::shared_ptr<ITextureResource> texture, const double angle, const Selection selection)
        : texture(texture), angle(angle), selection(selection) {}

    void Sprite::Draw(const Point &position) const {

        texture->Draw(selection, angle, position);
    }

    void Sprite::Draw(const Point &position, size_t width, size_t height) const {

        texture->Draw(selection, angle, position, width, height);
    }

    // TODO REMOVE
    SDL_Renderer* Sprite::GetRenderer() const {

        auto t = dynamic_cast<SdlWrapper::SdlTextureResource*>(texture.get());
        return t->renderer;
    }
}