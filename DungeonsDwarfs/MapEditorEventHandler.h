#pragma once
#include <functional>

// Engine
#include "IEventHandler.h"
#include "MouseClickEvent.h"
#include "MouseMoveEvent.h"
#include "QuitEvent.h"

// DungeonDwarfs
#include "GraphBuilder.h"

// DungeonDwarfsMapEditor
#include "Cursor.h"

namespace DungeonDwarfsMapEditor {

    class MapEditorEventHandler : public Engine::IEventHandler
    {
    private:
        const int hitDetectionMarginSize = 20;
        const size_t tileSize;
        const size_t screenHeight;

        Graphs::GraphBuilder &graphBuilder;
        Cursor &cursor;
        std::function<void()> quitAction;
        std::function<void()> rebuildMap;

    public:
        MapEditorEventHandler(std::function<void()> quitAction, 
                              std::function<void()> rebuildMap, 
                              Graphs::GraphBuilder &graphBuilder,
                              Cursor &cursor,
                              size_t tileSize, 
                              const size_t screenHeight);

        void Handle(const Engine::MouseClickEvent &e) const override;
        void Handle(const Engine::MouseMoveEvent &e) const override;
        void Handle(const Engine::QuitEvent &e) const override;

    private:
        Engine::Point GetCoordinate(const int x, const int y) const;

        void DispatchMouseEvent(int x, int y,
            std::function<void(Engine::Point&)> lowerHorizontal,
            std::function<void(Engine::Point&)> higherHorizontal,
            std::function<void(Engine::Point&)> lowerVertical,
            std::function<void(Engine::Point&)> higherVertical,
            std::function<void(Engine::Point&)> center) const;

        void HandleMarginClick(const Engine::Point from, const Engine::Point to) const;
        void HandleCenterClick(const Engine::Point from) const;
        bool IsEdgeValid(const Engine::Point &from, const Engine::Point &to) const;
        
        bool IsLowerMarginClick(const int primary, const int secondary) const;
        bool IsHigherMarginClick(const int primary, const int secondary) const;
        bool IsCenter(const int x, const int y) const;        
    };
}