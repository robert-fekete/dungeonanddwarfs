#include "DrawableTile.h"

// TODO Get rid of this
#include "SDL.h"
#include <iostream>

namespace DungeonDwarfs {

    DrawableTile::DrawableTile(const TileViewModel &tile, 
                                const Engine::Sprite sprite, 
                                const std::vector<Engine::Sprite> shadowSprites,
                                const size_t tileSize, 
                                const size_t screenHeight)
        : tileSize(tileSize), 
        screenHeight(screenHeight), 
        tile(tile), 
        sprite(sprite), 
        shadowSprites(shadowSprites)
    {}

    void DungeonDwarfs::DrawableTile::Draw() const{

        auto position = tile.GetPosition();
        position.Scale((int)tileSize).
            Scale(1, -1).
            Translate(0, (int)screenHeight - (int)tileSize);

        if (tile.IsVisible()) {
            sprite.Draw(position, tileSize, tileSize);
            
            //position.Translate(6, -10);
            auto direction = tile.GetShadowDirection();
            auto shadowIndex = GetShadowIndex(direction);
            if (direction == 0) {

                shadowSprites[shadowIndex].Draw(position, tileSize, tileSize);
                auto tilePosition = tile.GetPosition();
                if (!tile.HasNeighbor(tilePosition.CopyWithOffset(0, 1))) {
                    shadowSprites[5].Draw(position, tileSize, tileSize);
                }
                if (!tile.HasNeighbor(tilePosition.CopyWithOffset(1, 0))) {
                    shadowSprites[6].Draw(position, tileSize, tileSize);
                }
                if (!tile.HasNeighbor(tilePosition.CopyWithOffset(0, -1))) {
                    shadowSprites[7].Draw(position, tileSize, tileSize);
                }
                if (!tile.HasNeighbor(tilePosition.CopyWithOffset(-1, 0))) {
                    shadowSprites[8].Draw(position, tileSize, tileSize);
                }
            }
            else{

                shadowSprites[shadowIndex].Draw(position, tileSize, tileSize);

                auto shadowLevel = tile.GetShadowLevel();

                SDL_SetRenderDrawColor(sprite.GetRenderer(), 0, 0, 0, shadowLevel);
                //SDL_SetRenderDrawBlendMode(sprite.GetRenderer(), SDL_BLENDMODE_BLEND);
                SDL_Rect rect;
                rect.x = position.X();
                rect.y = position.Y();
                rect.w = (int) tileSize;
                rect.h = (int) tileSize;
                SDL_RenderFillRect(sprite.GetRenderer(), &rect);
            }
        }
    }
    
    int DrawableTile::GetShadowIndex(int direction) const {

        return direction;
    }
}