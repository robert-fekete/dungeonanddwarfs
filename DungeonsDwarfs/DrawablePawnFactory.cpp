#include "DrawablePawnFactory.h"

#include <vector>

// Engine
#include "Animation.h"
#include "Selection.h"


namespace DungeonDwarfs {

    DrawablePawnFactory::DrawablePawnFactory(std::shared_ptr<Engine::BaseResourceFactory> resourceFactory, 
                                            const size_t tileSize,
                                            const size_t screenHeight)
        : tileSize(tileSize),
        screenHeight(screenHeight),
        resourceFactory(resourceFactory)
    {
    }

    std::unique_ptr<DrawablePawn> DrawablePawnFactory::CreatePawn(PawnViewModel &pawn) {

        auto texture = resourceFactory->GetImageResource("..\\Resources\\starlord_mask.png");
        Engine::Sprite neutral(texture, Engine::Selection(0, 0, 32, 48));

        std::vector<Engine::Animation> animations;
        for (int i = 0; i < 4; i++) {
            
            std::vector<Engine::Selection> selections{ Engine::Selection(0, i * 48, 32, 48),
                                                       Engine::Selection(32, i * 48, 32, 48),
                                                       Engine::Selection(64, i * 48, 32, 48),
                                                       Engine::Selection(96, i * 48, 32, 48), };

            Engine::Animation animation(texture, selections, 6);

            animations.push_back(std::move(animation));
        }

        std::unique_ptr<DrawablePawn> ptr(new DrawablePawn(pawn, std::move(neutral), std::move(animations), tileSize, screenHeight));

        return std::move(ptr);
    }
}