#include <iostream>
#include <memory>
#include <vector>
#include "SDL.h"

#include "ArgumentParser.h"

// SdlWrapper
#include "SdlFramework.h"

// DungeonDwarfs
#include "DungeonGame.h"

// DungeonDwarfsMapEditor
#include "MapEditor.h"

// Graphs
#include "GraphBuilder.h"
#include "GraphSerializer.h"


void PrintHelp() {

    std::cout << "Dungeons and Dwarfs" << std::endl;
    std::cout << "Arguments:" << std::endl;
    std::cout << "--help\t\t\tShows this help" << std::endl;
    std::cout << "--mapEditor\t\tOpens up the map editor" << std::endl;
    std::cout << "--inputMap=<path>\tLoads a map from the specified file" << std::endl;
    std::cout << "--outputMap=<path>\tSaves the map in the specified file. Only used in map editr mode." << std::endl;
}


int main(int argc, char * argv[]) {
	
    std::unordered_set<std::string> validArguments{
        "--help",
        "--mapEditor",
        "--inputMap",
        "--outputMap"
    };
    ArgumentParser parser(argc, argv, validArguments);
    if (!parser.IsValid() || parser.HasArgument("--help")) {
        
        PrintHelp();
        return 1;
    }

    try {
        std::unique_ptr<Engine::IFramework> ptr(new SdlWrapper::SdlFramework(std::cout, 640, 640));
        
        if (!parser.HasArgument("--mapEditor")) {

            Graphs::GraphBuilder graphBuilder;
            if (parser.HasArgument("--inputMap")) {

                Graphs::GraphSerializer inputSerializer(parser.GetArgumentValue("--inputMap"));
                graphBuilder = Graphs::GraphBuilder::Deserialize(inputSerializer);
            }
            DungeonDwarfs::DungeonGame game(std::move(ptr), graphBuilder.Build());
            game.Run();
        }
        else {

            Graphs::GraphBuilder graphBuilder;
            if (parser.HasArgument("--inputMap")) {

                Graphs::GraphSerializer inputSerializer(parser.GetArgumentValue("--inputMap"));
                graphBuilder = Graphs::GraphBuilder::Deserialize(inputSerializer);
            }

            DungeonDwarfsMapEditor::MapEditor editor(std::move(ptr), graphBuilder);
            editor.Run();

            if (parser.HasArgument("--outputMap")) {
                Graphs::GraphSerializer outputSerializer(parser.GetArgumentValue("--outputMap"));
                graphBuilder.Serialize(outputSerializer);
            }
        }
    }
    catch (std::runtime_error &exception) {
        std::cout << exception.what() << std::endl;
    }

	return 0;
}