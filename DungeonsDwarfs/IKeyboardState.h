#pragma once
#include "KeyboardConstants.h"

namespace Events {

    class IKeyboardState
    {
    public:
        virtual bool IsPressed(Scancode key) const = 0;
    };
}