#pragma once

namespace Engine {
 
    class Selection {
    public:
        int X;
        int Y;
        size_t Width;
        size_t Height;

        Selection()
            : X(0), Y(0), Width(0), Height(0) {}

        Selection(int x, int y, size_t width, size_t height)
            : X(x), Y(y), Width(width), Height(height) {}
    };
}