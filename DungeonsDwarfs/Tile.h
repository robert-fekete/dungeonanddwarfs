#pragma once
#include <utility>

// Graphs
#include "Graph.h"

// Engine
#include "Point.h"

namespace DungeonDwarfs {

    class Tile
    {
    private:
        const Engine::Point coordinate;
        const Graphs::Graph &graph;

    public:
        bool IsVoid;
        const unsigned int NumberOfNeighbors = 0;

    public:
        Tile(const Engine::Point coordinate, const Graphs::Graph &graph, bool isVoid);
        Engine::Point GetPosition() const;
        bool HasNeighbor(const Engine::Point &neighbor) const;
    };
}
