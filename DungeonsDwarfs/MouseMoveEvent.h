#pragma once
#include "Event.h"
#include "IEventHandler.h"

namespace Engine {

    class MouseMoveEvent : public Event
    {
    public:
        const int X;
        const int Y;
        const bool IsLeftPressed;
        const bool IsRightPressed;

    public:
        MouseMoveEvent(int x, int y, bool isLeftPressed, bool isRightPressed);
        void Accept(const IEventHandler &handler) const override;
    };
}