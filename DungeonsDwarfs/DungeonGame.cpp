#include "DungeonGame.h"


namespace DungeonDwarfs {

    DungeonGame::DungeonGame(std::unique_ptr<Engine::IFramework> framework_, Graphs::Graph map_)
        : Game(std::move(framework_)),
        map(map_),
        tileFactory(DrawableTileFactory(framework->GetResourceFactory(), tileSize, framework->GetScreenHeight())),
        pawnFactory(DrawablePawnFactory(framework->GetResourceFactory(), tileSize, framework->GetScreenHeight())),
        shadowFactory(DrawableShadowFactory(framework->GetResourceFactory(), tileSize, framework->GetScreenHeight())),
        pawn(Engine::Point(1, 1), map),
        pawnViewModel(pawn, framework->GetKeyboardProvider(), tileSize),
        eventHandler(EventHandler(std::bind(&DungeonGame::Stop, this), pawnViewModel))
    {
        framework->SetBackgroundColor(Engine::Color(0, 0, 0, 255));

        for (auto const &vertex : map.GetVertices()){

            Tile tile(vertex, map, false);
            tiles.push_back(std::make_unique<TileViewModel>(tile, pawn, map));
        }

        RegisterTiles();
        RegisterPawn();
        //RegisterShadow();
    }

    void DungeonGame::RegisterTiles() {

        for (auto &tile : tiles) {
            drawableTiles.push_back(tileFactory.CreateTile(*tile));
        }

        for (auto &tile : tiles) {
            updatables.Add(tile.get());
        }

        for (auto &drawableTile : drawableTiles) {
            drawables.Add(&drawableTile);
        }
    }

    void DungeonGame::RegisterPawn() {

        drawablePawn = pawnFactory.CreatePawn(pawnViewModel);

        updatables.Add(drawablePawn.get());
        updatables.Add(&pawnViewModel);
        drawables.Add(drawablePawn.get());
    }


    // TODO remove
    void DungeonGame::RegisterShadow() {

        drawableShadow = shadowFactory.CreateShadow(pawnViewModel, pawn, map);

        drawables.Add(drawableShadow.get());
    }

    const Engine::IEventHandler& DungeonGame::GetEventHandler() const {

        return eventHandler;
    }

    DungeonGame::~DungeonGame()
    {
    }
}