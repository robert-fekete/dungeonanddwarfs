#pragma once
#include <memory>
#include "Event.h"
#include "IFramework.h"
#include "DrawablesCollection.h"
#include "UpdatablesCollection.h"

namespace Engine {

    class Game
    {
    protected:
        std::unique_ptr<IFramework> framework;
        DrawablesCollection drawables;
        UpdatablesCollection updatables;

    private:
        bool isRunning = false;
        unsigned int currentTime;

    public:
        Game(std::unique_ptr<IFramework> framework);
        void Run();
        virtual ~Game() {}

    protected:
        void Stop();

    private:
        virtual const IEventHandler& GetEventHandler() const = 0;
    };
}
