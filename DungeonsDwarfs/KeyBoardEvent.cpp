#include "KeyboardEvent.h"


namespace Engine {

    KeyboardEvent::KeyboardEvent(Events::Key key, unsigned int modifiers, bool isPressed, bool repeat)
        : Key(key), Modifiers(modifiers), IsPressed(isPressed), Repeat(repeat)
    {
    }

    void KeyboardEvent::Accept(const IEventHandler &handler) const {

        handler.Handle(*this);
    }
}