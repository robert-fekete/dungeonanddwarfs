#pragma once
#include <fstream>
#include <string>
#include <unordered_set>

// Graphs
#include "Edge.h"

// Engine
#include "Point.h"

namespace Graphs {
 
    class GraphSerializer
    {
    private:
        const char coordinateSeparator = ',';
        const char pointSeparator = ':';

        const std::string path;
    public:
        GraphSerializer(const std::string path);
        void Serialize(const std::unordered_set<Engine::Point> &vertices, const std::unordered_set<Edge> &edges) const;
        void Deserialize(std::unordered_set<Engine::Point> &vertices, std::unordered_set<Edge> &edges) const;
    };
}