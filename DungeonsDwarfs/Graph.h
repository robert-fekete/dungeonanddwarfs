#pragma once
#include <vector>
#include <unordered_map>
#include <unordered_set>

// Graphs
#include "Edge.h"

// Engine
#include "Point.h"

namespace Graphs {

    class Graph 
    {
    private:
        std::unordered_set<Engine::Point> vertices;
        std::unordered_map<Engine::Point, std::unordered_set<Engine::Point>> edges;
    public:
        Graph(std::unordered_set<Engine::Point> vertices, std::unordered_set<Edge> edges);

        std::unordered_set<Engine::Point> GetVertices() const;
        bool HasEdge(const Engine::Point &from, const Engine::Point &to) const;
    };
}