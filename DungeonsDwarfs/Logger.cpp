#include "Logger.h"


Engine::Logger::Logger(std::ostream &output)
    : output(output)
{ }

void Engine::Logger::LogMessage(const std::string &message, const std::string &detail) const {

    output << message << detail << std::endl;
}

void Engine::Logger::LogException(const std::exception &exception, const std::string &detail) const {

    output << exception.what() << ": " << detail << std::endl;
}